package com.example.android.ble_android_cane;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.androidplot.xy.XYPlot;

public class PlotFragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";

    private String mParam1;

    //public XYPlot plot;

    public PlotFragment() {
        // Required empty public constructor
    }

    public static PlotFragment newInstance(boolean recording) {
        PlotFragment fragment = new PlotFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, Boolean.toString(recording));
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
        }
        this.setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.plot_frag, container, false);
        //plot = rootView.findViewById(R.id.plot);
        return rootView;
    }
}
