package com.example.android.ble_android_cane;

import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;
import android.view.WindowManager;
import android.support.v7.widget.Toolbar;

import com.androidplot.xy.BarFormatter;
import com.androidplot.xy.BarRenderer;
import com.androidplot.xy.BoundaryMode;
import com.androidplot.xy.LineAndPointFormatter;
import com.androidplot.xy.PointLabelFormatter;
import com.androidplot.xy.SimpleXYSeries;
import com.androidplot.xy.StepMode;
import com.androidplot.xy.XYPlot;
import com.androidplot.xy.XYSeries;

import java.util.ArrayList;
import java.util.Arrays;

import eu.darken.myolib.BaseMyo;
import eu.darken.myolib.Myo;
import eu.darken.myolib.MyoCmds;
import eu.darken.myolib.msgs.MyoMsg;

public class MainActivity extends AppCompatActivity {
    private final static String TAG = MainActivity.class.getSimpleName();
    BluetoothLeService mBluetoothLeService;
    FeedbackService mFeedbackService;
    String trialID = "trial0";
    DBHelper dbHelper;
    Myo myo;
    boolean vibLock = false;
    long lastDBCheckTime = 0;
    long dbCheckLockTime = 2000;

    public final static String ACTION_NEW_PAGE = "com.example.bluetooth.le.ACTION_NEW_PAGE";
    public final static String ACTION_TRIGGER = "com.example.bluetooth.le.ACTION_TRIGGER";
    public final static String EXTRA_TRIGGER = "com.example.bluetooth.le.ACTION_TRIGGER";

    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;
    private static Button recordButton;
    private static TextView loadText;
    private static XYPlot plot;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        Intent gattServiceIntent = new Intent(this, BluetoothLeService.class);
        bindService(gattServiceIntent, mServiceConnection, BIND_AUTO_CREATE);
        Intent feedbackServiceIntent = new Intent(this, FeedbackService.class);
        bindService(feedbackServiceIntent, mFeedbackServiceConnection, BIND_AUTO_CREATE);

        IntentFilter filter = new IntentFilter();
        filter.addAction(BluetoothLeService.ACTION_FEEDBACK);
        filter.addAction(BluetoothLeService.ACTION_GATT_DISCONNECTED);
        registerReceiver(receiver, filter);

        boolean hasLowLatencyFeature =
                getPackageManager().hasSystemFeature(PackageManager.FEATURE_AUDIO_LOW_LATENCY);

        boolean hasProFeature =
                getPackageManager().hasSystemFeature(PackageManager.FEATURE_AUDIO_PRO);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager){
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                super.onTabSelected(tab);
                if(tab.getPosition() < 1){
                    setButtons(mBluetoothLeService);
                }else{
                    updatePlot();
                }
            }
        });

        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
    }

    public final BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if(action.equalsIgnoreCase(BluetoothLeService.ACTION_GATT_DISCONNECTED)){
                Log.d(TAG, "onReceive: Gatt disconnected");
                setButtons(mBluetoothLeService);
            }
            else if(action.equalsIgnoreCase(FeedbackService.ACTION_PLOT)) {
                updatePlot();
            }else if(action.equalsIgnoreCase(FeedbackService.ACTION_UI)) {
                double loadie = intent.getDoubleExtra(FeedbackService.EXTRA_LOAD,0);
                updateUI(loadie);
            }else if(action.equalsIgnoreCase(MainActivity.ACTION_NEW_PAGE)) {
                try {
                    setButtons(mBluetoothLeService);
                }catch(Exception e){}
            }
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        MenuItem exportItem = menu.findItem(R.id.menu_export);
        exportItem.setVisible(false);
        invalidateOptionsMenu();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.menu_devices:
                final Intent intent = new Intent(this, DeviceControlActivity.class);
                startActivity(intent);
                return true;
            case R.id.menu_export:
                try {
                    if (mBluetoothLeService.recording == false) {
                        EditText partID = findViewById(R.id.part_id);
                        try{
                            int bw = (int) mFeedbackService.getBodyWeight();
                            if(mFeedbackService.getBodyWeight() % 1 >= 0.5){
                                bw++;
                            }
                            String bwString = String.format("%03d",bw); //BW in lbs
                            double targ = mFeedbackService.getTargetPercentBW();
                            String targString = String.format("%.2f",targ);
                            DBExport.startActionExportDB(this, Long.toString(100000), partID.getText().toString(),bwString,targString);
                        }catch(Exception e){

                        }

                        return true;
                    }else{
                        Toast.makeText(this, "Can't export while recording data!", Toast.LENGTH_LONG).show();
                        return false;
                    }
                }catch(Exception e){
                    return false;
                }
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    // Code to manage Service lifecycle.
    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
            mBluetoothLeService = ((BluetoothLeService.LocalBinder) service).getService();
            if (!mBluetoothLeService.initialize()) {
                Log.e(TAG, "Unable to initialize Bluetooth");
                finish();
            }
            setButtons(mBluetoothLeService);
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mBluetoothLeService = null;
        }
    };

    private final ServiceConnection mFeedbackServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
            mFeedbackService = ((FeedbackService.LocalBinder) service).getService();
            if (!mFeedbackService.initialize()) {
                Log.e(TAG, "Unable to initialize Bluetooth");
                finish();
            }
            setButtons(mBluetoothLeService);
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mFeedbackService = null;
        }
    };

    @Override
    protected void onDestroy() {
        try {
            unregisterReceiver(receiver);
            unbindService(mServiceConnection);
            unbindService(mFeedbackServiceConnection);
        }catch(Exception e){}
        mBluetoothLeService = null;
        mFeedbackService = null;
        super.onDestroy();
    }

    @Override
    protected void onResume(){
        IntentFilter filter = new IntentFilter();
        filter.addAction(BluetoothLeService.ACTION_FEEDBACK);
        filter.addAction(BluetoothLeService.ACTION_GATT_DISCONNECTED);
        filter.addAction(FeedbackService.ACTION_PLOT);
        filter.addAction(FeedbackService.ACTION_UI);
        registerReceiver(receiver, filter);
        setButtons(mBluetoothLeService);

        //feedbackCondition = 0;
        vibLock = true;
        if(myo != null) {
            myo.writeVibrate(MyoCmds.VibrateType.NONE, new Myo.MyoCommandCallback() {
                @Override
                public void onCommandDone(Myo myo, MyoMsg msg) {
                    //Log.d(TAG, "onCommandDone: Vibration "+msg.toString());
                    vibLock = false;
                }
            });
        }else{
            vibLock = false;
        }
        //handler = new Handler();
        super.onResume();
    }

    @Override
    protected void onPause(){
        unregisterReceiver(receiver);
        super.onPause();
    }

    private void broadcastUpdate(final String action, boolean extra) {
        final Intent intent = new Intent(action);
        intent.putExtra(MainActivity.EXTRA_TRIGGER, extra);
        sendBroadcast(intent);
    }

    public void countButtonClicked(View view) {
        if ((System.currentTimeMillis() - lastDBCheckTime) > dbCheckLockTime){
            dbHelper = DBHelper.getInstance(this);
            int count = dbHelper.getCaneTableCount();
            lastDBCheckTime = System.currentTimeMillis();
            Toast.makeText(this, "Database Row Count: " + count, Toast.LENGTH_SHORT).show();
        }
    }

    public void recordButtonClicked(View view){
        boolean switchSuccess = mBluetoothLeService.recordingSwitch();
        Button recordButton = findViewById(R.id.recording_button);
        if(switchSuccess){
            recordButton = findViewById(R.id.recording_button);
            if(mBluetoothLeService.recording){
                recordButton.setText("Stop Recording");
                recordButton.setBackgroundColor(getResources().getColor(R.color.connected));
            }else{
                recordButton.setText("Record");
                recordButton.setBackgroundColor(getResources().getColor(R.color.disconnected));
            }
        }else{
            if(!mBluetoothLeService.recording){
                recordButton.setText("Record");
                recordButton.setBackgroundColor(getResources().getColor(R.color.disconnected));
            }
        }
    }

    //Use volume buttons to turn trigger on/off
    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        int action = event.getAction();
        int keyCode = event.getKeyCode();
        switch (keyCode) {
            case KeyEvent.KEYCODE_VOLUME_UP:
                if (action == KeyEvent.ACTION_DOWN) {
                    triggerButtonClicked(null);
                }
                return true;
            case KeyEvent.KEYCODE_VOLUME_DOWN:
                if (action == KeyEvent.ACTION_DOWN) {
                    triggerButtonClicked(null);
                }
                return true;
            default:
                return super.dispatchKeyEvent(event);
        }
    }

    @Override
    public void onBackPressed() {
        return;
        //moveTaskToBack(true);
    }

    public void triggerButtonClicked(View view){
        //triggerButtonUpdate(false); //Used when trigger is turned on/off

        //Turn trigger on for TIME ms
        try {
            mBluetoothLeService.trigger = true;
            mFeedbackService.trigger = true;
            Button triggerButton = findViewById(R.id.trigger_button);
            triggerButton.setText("On");
            triggerButton.setBackgroundColor(Color.GREEN);
        }catch (Exception e){
            Log.d(TAG, "triggerButtonUpdate: error updating trigger button");
        }

        int TIME = 2000; //50 ms

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    mBluetoothLeService.trigger = false;
                    mFeedbackService.trigger = false;
                    Button triggerButton = findViewById(R.id.trigger_button);
                    triggerButton.setText("Off");
                    triggerButton.setBackgroundColor(Color.RED);
                }catch (Exception e){
                    Log.d(TAG, "triggerButtonUpdate: error updating trigger button");
                }
            }
        }, TIME);
    }

    //Refreshes the trigger button when keep is true, swaps trigger value when false
    private void triggerButtonUpdate(boolean keep){
        try {
            Button triggerButton = findViewById(R.id.trigger_button);
            if (mBluetoothLeService.trigger ^ keep) {
                triggerButton.setText("Off");
                triggerButton.setBackgroundColor(Color.RED);
                mBluetoothLeService.trigger = false;
                mFeedbackService.trigger = false;
                //broadcastUpdate(MainActivity.ACTION_TRIGGER,false);

            } else {
                triggerButton.setText("On");
                triggerButton.setBackgroundColor(Color.GREEN);
                mBluetoothLeService.trigger = true;
                mFeedbackService.trigger = true;
                //broadcastUpdate(MainActivity.ACTION_TRIGGER,false);
            }
        }catch (Exception e){
            Log.d(TAG, "triggerButtonUpdate: error updating trigger button");
        }
    }

    public void updateUI(double loadIn){
        TextView loadText = null;
        try{
            loadText = findViewById(R.id.load);
        }catch(Exception e){}
        int feedbackCondition = mFeedbackService.getFeedbackCondition();
        switch(feedbackCondition) {
            case 0:

                break;
            case 1: //scale
                try {
                    loadText.setText(String.format("Load: %.1f", loadIn));
                }catch (Exception e){
                    Log.d(TAG, "updateUI: "+e.getMessage());
                }
                break;
            case 2: //Audio
                try {
                    if (loadIn > 10) {
                        loadText.setText(String.format("Load: %.2f", loadIn));
                    } else {
                        loadText.setText("");
                    }
                }catch(Exception e){}
                break;
            case 3: //Summary

                break;
            case 4: //Myo haptic
                if(myo != null) {
                    myo.writeVibrate(MyoCmds.VibrateType.LONG, new Myo.MyoCommandCallback() {
                        @Override
                        public void onCommandDone(Myo myo, MyoMsg msg) {
                            //Log.d(TAG, "onCommandDone: Vibration "+msg.toString());
                            vibLock = false;
                        }
                    });
                }else{
                    connectMyo();
                }
                break;
            case 5: //Summary plot
                setBodyWeight();
                mFeedbackService.updatePlot();
                break;
            case 6: //Terminal Scale
                try {
                    loadText.setText(String.format("Load: %.2f", loadIn));
                }catch (Exception e){
                    Log.d(TAG, "updateUI: "+e.getMessage());
                }
                break;
        }
    }

    public void setBodyWeight(){
        try{
            EditText bwText = findViewById(R.id.bodyweight);
            String bwString = bwText.getText().toString();
            double bodyweight = Double.parseDouble(bwString);
            mFeedbackService.setBodyWeight(bodyweight);
        }catch(Exception e){}
    }

    public void onTargetButtonClicked(View view) {
        boolean checked = ((RadioButton) view).isChecked();
        switch(view.getId()) {
            case R.id.radio_targ_10:
                if (checked) {
                    mFeedbackService.setTargetPercentBW(0.1);
                }
                break;
            case R.id.radio_targ_12:
                if (checked) {
                    mFeedbackService.setTargetPercentBW(0.12);
                }
                break;
            case R.id.radio_targ_14:
                if (checked) {
                    mFeedbackService.setTargetPercentBW(0.14);
                }
                break;
        }
        setBodyWeight();
        setTargetRadios();
    }

    public void setTargetRadios(){
        try {
            RadioButton targ10 = findViewById(R.id.radio_targ_10);
            RadioButton targ12 = findViewById(R.id.radio_targ_12);
            RadioButton targ14 = findViewById(R.id.radio_targ_14);

            double bodyWeight = mFeedbackService.getBodyWeight();

            if(bodyWeight > 0) {
                targ10.setText(String.format("%.1f", 0.1 * bodyWeight));
                targ12.setText(String.format("%.1f", 0.12 * bodyWeight));
                targ14.setText(String.format("%.1f", 0.14 * bodyWeight));
            }else{
                targ10.setText(String.format("10%"));
                targ12.setText(String.format("12%"));
                targ14.setText(String.format("14"));
            }

        }catch(Exception e){}
    }

    public void onRadioButtonClicked(View view) {

        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();
        mFeedbackService.resetCondition();
        setBodyWeight();
        setTargetRadios();

        if(myo != null)
            myo.writeVibrate(MyoCmds.VibrateType.NONE,null);

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.radio_off:
                if (checked) {
                    //mBluetoothLeService.trigger = false;
                    TextView loadText = findViewById(R.id.load);
                    loadText.setText("No Feedback");
                    //feedbackCondition = 0;
                    mFeedbackService.setFeedbackCondition(0);
                }
                break;
            case R.id.radio_scale:
                if (checked)
                    //mBluetoothLeService.trigger = true;
                    //feedbackCondition = 1;
                    mFeedbackService.setFeedbackCondition(1);
                break;
            case R.id.radio_scale_terminal:
                if (checked)
                    //mBluetoothLeService.trigger = true;
                    //feedbackCondition = 1;
                    mFeedbackService.setFeedbackCondition(6);
                break;

            case R.id.radio_audio:
                if (checked)
                    //mBluetoothLeService.trigger = true;
                    //feedbackCondition = 2;
                    mFeedbackService.setFeedbackCondition(2);
                break;
            case R.id.radio_summary:
                if (checked)
                    //feedbackCondition = 3;
                    mFeedbackService.setFeedbackCondition(3);
                    mFeedbackService.resetStrides();
                break;
            /*case R.id.radio_haptic:
                if (checked)
                    //feedbackCondition = 4;
                    mFeedbackService.setFeedbackCondition(4);
                    connectMyo();
                break;
            */
            case R.id.radio_summary_long:
                if (checked)
                    //feedbackCondition = 5;
                    mFeedbackService.setFeedbackCondition(5);
                break;
        }
    }

    private void connectMyo(){
        BluetoothDevice dev = mBluetoothLeService.getMyoDevice();
        if(dev == null){
            Toast.makeText(this,"Not connected to a Myo band!",Toast.LENGTH_SHORT).show();
            myo = null;
        }else{
            myo = new Myo(this,dev);
            if(myo.getConnectionState().equals(BaseMyo.ConnectionState.DISCONNECTED)) {
                myo.connect();
                myo.writeSleepMode(MyoCmds.SleepMode.NEVER,null);
            }
        }
    }

    public void setButtons(BluetoothLeService mBluetoothLeService){
        try {
            recordButton = findViewById(R.id.recording_button);
            loadText = findViewById(R.id.load);
            if (mBluetoothLeService == null) {
                recordButton.setText("Record");
                recordButton.setBackgroundColor(getResources().getColor(R.color.disconnected));
            } else {
                if (mBluetoothLeService.recording) {
                    recordButton.setText("Stop Recording");
                    recordButton.setBackgroundColor(getResources().getColor(R.color.connected));
                } else {
                    recordButton.setText("Record");
                    recordButton.setBackgroundColor(getResources().getColor(R.color.disconnected));
                    loadText.setText("Disconnected");
                }
            }
        }catch(Exception e){
            Log.e(TAG, "setButtons: "+e.getMessage() );
        }
        triggerButtonUpdate(true);
    }

    public void updatePlot(){
        try {
            ArrayList<Double> strides = mFeedbackService.getStrides();
            double loadTarget = mFeedbackService.getTarget();
            Number[] yVals = new Number[strides.size()];//{10,20,30};
            Number[] targetVals = {loadTarget,loadTarget,loadTarget,loadTarget,loadTarget,loadTarget,loadTarget,loadTarget,loadTarget,loadTarget,loadTarget};
            Number[] targetX = {0,1,2,3,4,5,6,7,8,9,10};
            Number[] xVals = new Number[strides.size()];//{};
            for(int i = 0; i < strides.size(); i++){
                xVals[i] = i+1;
                yVals[i] = strides.get(i);
                targetVals[i] = loadTarget;
            }
            //XYSeries series = new SimpleXYSeries(Arrays.asList(yVals),SimpleXYSeries.ArrayFormat.Y_VALS_ONLY, "Peak Load");
            XYSeries series = new SimpleXYSeries(Arrays.asList(xVals),Arrays.asList(yVals), "Peak Load");
            XYSeries targets = new SimpleXYSeries(Arrays.asList(targetX),Arrays.asList(targetVals), "Target");
            LineAndPointFormatter sFormat = new LineAndPointFormatter(
                    Color.rgb(0, 200, 0),                   // line color
                    Color.rgb(0, 100, 0),                   // point color
                    Color.rgb(0, 0, 0),               // fill color
                    new PointLabelFormatter(150));
            Paint transparent = new Paint();
            transparent.setAlpha(0);
            sFormat.setFillPaint(transparent);
            sFormat.setVertexPaint(transparent);
            sFormat.getLinePaint().setStrokeWidth(10);
            plot.clear();
            plot.setRangeBoundaries(0,50, BoundaryMode.FIXED);
            plot.setDomainBoundaries(0,10, BoundaryMode.FIXED);
            plot.setLinesPerRangeLabel(1);
            plot.setRangeStep(StepMode.INCREMENT_BY_VAL,10);
            plot.setLinesPerDomainLabel(1);
            plot.setDomainStep(StepMode.INCREMENT_BY_VAL,1);
            plot.setRangeLabel("Load (lb)");
            plot.setDomainLabel("Stride #");
            plot.setTitle("Stride loads");

            BarFormatter barFormat = new BarFormatter(getResources().getColor(R.color.colorPrimary), Color.BLACK);
            plot.addSeries(series, barFormat);
            BarRenderer renderer = plot.getRenderer(BarRenderer.class);
            renderer.setBarGroupWidth(BarRenderer.BarGroupWidthMode.FIXED_WIDTH, 50);

            plot.addSeries(targets, sFormat);
            plot.redraw();
        }catch(Exception e){
            Log.e(TAG, "updatePlot: "+e.getMessage() );
        }
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    /*TODO: Note that to re-introduce feedback, uncomment the sections below referencing section 1 and switch page count to 2.
        TODO: Also uncomment tabs and set visibility of tabs in activity_main.xml, uncomment feedback rendering method in this activity,
        TODO: and uncomment the feedback radio buttons in weight_frag.
     */
    public static class PlaceholderFragment extends Fragment {

        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_weight, container, false);
            int pageNumber = (getArguments().getInt(ARG_SECTION_NUMBER)) - 1;

            RadioButton feedbackOff = rootView.findViewById(R.id.radio_off);
            try {
                feedbackOff.setChecked(true);
            }catch(Exception e){}

            switch(pageNumber) {
                //First fragment, used for app diagnostic purposes
                case 0:
                    rootView = inflater.inflate(R.layout.fragment_weight, container, false);
                    break;
                //Plot fragment
                case 1:
                    rootView = inflater.inflate(R.layout.plot_frag, container, false);
                    plot = rootView.findViewById(R.id.plot);
                    break;
                //Scale/terminal fragment
                case 2:
                    rootView = inflater.inflate(R.layout.scale_frag, container, false);
                    break;
            }
            return rootView;
        }

    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).

            Fragment frag = PlaceholderFragment.newInstance(position + 1);
            switch(position) {
                case 0:
                    break;
                case 1:
                    updatePlot();
                    break;
                case 2:
                    setButtons(mBluetoothLeService);
                    break;
                default:
                    break;
            }
            return frag;
        }

        @Override
        public int getCount() {
            // Show 2 total pages.
            return 1;
        }

    }
}
