package com.example.android.ble_android_cane;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

public class WeightFragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";

    private String mParam1;

    Button recordingButton;
    TextView loadText;

    public WeightFragment() {
        // Required empty public constructor
    }

    public static WeightFragment newInstance(boolean recording) {
        WeightFragment fragment = new WeightFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, Boolean.toString(recording));
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
        }
        this.setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_weight, container, false);
        recordingButton = rootView.findViewById(R.id.recording_button);
        loadText = rootView.findViewById(R.id.load);
        if(recordingButton != null) {
            if (Boolean.parseBoolean(mParam1)) {
                recordingButton.setText("Stop Recording");
                recordingButton.setBackgroundColor(Color.RED);
            } else {
                recordingButton.setText("Record");
                recordingButton.setBackgroundColor(Color.GREEN);
            }
        }
        return rootView;
    }
}
