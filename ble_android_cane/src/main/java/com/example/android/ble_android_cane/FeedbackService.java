/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.android.ble_android_cane;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import java.io.File;
import java.util.ArrayList;

import static java.lang.Math.floor;

/**
 * Service for managing feedback from tablet in the background. Accepts load and renders audio,
 * Broadcasts back to MainActivity for manipulation of visual displays.
 */
public class FeedbackService extends Service{
    private final static String TAG = FeedbackService.class.getSimpleName();
    private int feedbackCondition = 0;
    int frameCount = 0;
    int strideNum;
    int zeroDuration;
    double strideMax;
    double strideMaxSum;
    int zeroThreshold = 40;
    public static double loadThreshold = 3.0; //Minimum non-zero load
    double bodyWeight = 0;
    double targetPercentBW = 0.10;
    double target = 0;
    double successWidth = 0.025;
    int strideDuration;
    public boolean trigger = false;
    double runningSum = 0;
    int counter = 0;
    double prevLoad = 0;
    ArrayList<Double> strides = new ArrayList<Double>();
    MediaPlayer mediaPlayerHigh;
    MediaPlayer mediaPlayerLow;
    Handler handler;
    int sampleRate = 44100; // Feel free to change this
    int duration = 5;   //Can't create track if this is too big (buffer too big)
    double frequency = 392.0;

    public final static String ACTION_PLOT =
            "com.example.bluetooth.le.ACTION_PLOT";
    public final static String ACTION_UI =
            "com.example.bluetooth.le.ACTION_UI";
    public final static String ACTION_RESET =
            "com.example.bluetooth.le.ACTION_RESET";
    public final static String EXTRA_LOAD =
            "com.example.bluetooth.le.EXTRA_LOAD";

    public final BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if (action.equalsIgnoreCase(BluetoothLeService.ACTION_FEEDBACK)) {
                //Log.d(TAG, "onReceive: Ind: "+intent.getIntExtra(BluetoothLeService.EXTRA_IND,0)+", Time: "+System.currentTimeMillis());
                double loadExtra = 0;
                try {
                    loadExtra = intent.getDoubleExtra(BluetoothLeService.EXTRA_LOAD, -1);
                    renderFeedback(loadExtra, intent.getIntExtra(BluetoothLeService.EXTRA_IND, 0));
                } catch (Exception e) {
                    Log.e(TAG, "onReceive: " + Double.toString(loadExtra), e);
                }
            }
        }
    };

    public class LocalBinder extends Binder {
        FeedbackService getService() {
            return FeedbackService.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        handler = new Handler();
        return mBinder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        mediaPlayerHigh.release();
        mediaPlayerLow.release();
        return super.onUnbind(intent);
    }

    private final IBinder mBinder = new LocalBinder();

    public boolean initialize() {
        String path;
        String RESOURCE_PATH = ContentResolver.SCHEME_ANDROID_RESOURCE + "://";
        int resID = getResources().getIdentifier("sine_high", "raw", getPackageName());
        path = RESOURCE_PATH + getPackageName() + File.separator + resID;

        Uri uriHigh = Uri.parse(path);
        mediaPlayerHigh = new MediaPlayer();
        mediaPlayerHigh.setAudioStreamType(AudioManager.STREAM_MUSIC);
        try {
            mediaPlayerHigh.setDataSource(getApplicationContext(), uriHigh);
            mediaPlayerHigh.prepare();
            mediaPlayerHigh.start();
            mediaPlayerHigh.setLooping(true);
            mediaPlayerHigh.setVolume(0,0);
        }catch(Exception e){
            Log.d(TAG, "onCreate: Issue startign mediaplayer "+e.getMessage());
        }

        resID = getResources().getIdentifier("sine_low", "raw", getPackageName());
        path = RESOURCE_PATH + getPackageName() + File.separator + resID;
        Uri uriLow = Uri.parse(path);
        mediaPlayerLow = new MediaPlayer();
        mediaPlayerLow.setAudioStreamType(AudioManager.STREAM_MUSIC);
        try {
            mediaPlayerLow.setDataSource(getApplicationContext(), uriLow);
            mediaPlayerLow.prepare();
            mediaPlayerLow.start();
            mediaPlayerLow.setLooping(true);
            mediaPlayerLow.setVolume(0,0);
        }catch(Exception e){
            Log.d(TAG, "onCreate: Issue startign mediaplayer "+e.getMessage());
        }

        feedbackCondition = 0;
        IntentFilter filter = new IntentFilter();
        filter.addAction(BluetoothLeService.ACTION_FEEDBACK);
        registerReceiver(receiver, filter);
        return true;
    }

    public void updateUI(double load){
        broadcastUpdate(ACTION_UI, load);
    }

    public void updatePlot(){
        broadcastUpdate(ACTION_PLOT, -1);
    }

    private void broadcastUpdate(final String action, double extra) {
        final Intent intent = new Intent(action);
        intent.putExtra(FeedbackService.EXTRA_LOAD, extra);
        sendBroadcast(intent);
    }


    public void renderFeedback(double loadIn, int indi){
        double load = loadIn;//DataRead.getLoad((int) loadIn);
        switch(feedbackCondition) {
            case 0:

                break;
            case 1: //scale
                //TODO: Smooth signal somehow
                if (frameCount % 7 == 0) {
                    load = floor(load * 10) / 10;
                    Log.d(TAG, "renderFeedback Scale: "+indi+", Time: "+System.currentTimeMillis());
                }
                break;
            case 2: //Audio
                if (frameCount % 1 == 0) {
                    load = floor(load * 10) / 10;
                    Log.d(TAG, "renderFeedback Audio: " + indi + ", Time: " + System.currentTimeMillis());
                    generateSound(load, indi);
                }
                break;
            case 3: //Summary
                summarySegmentation(load, true);
                break;
            case 4: //Myo haptic

                break;
            case 5: //Summary plot
                summarySegmentation(load, false);
                break;
            case 6: //Scale terminal
                if(trigger == true){
                    if (frameCount % 7 == 0) {
                        load = floor(load * 10) / 10;
                        Log.d(TAG, "renderFeedback Terminal: "+indi+", Time: "+System.currentTimeMillis());
                    }
                    runningSum += load;
                    counter++;
                }else{
                    if(counter > 0){
                        prevLoad = runningSum/((double)counter);
                        counter = 0;
                        runningSum = 0;
                    }
                    load = prevLoad;
                }

                break;
        }
        if(frameCount % 7 == 0){
            updateUI(load);
        }
        frameCount++;
    }

    private void summarySegmentation(double load, boolean audioOn){
        if(load > strideMax)
            strideMax = load;
        if(load < loadThreshold) {
            zeroDuration++;
            if(zeroDuration > zeroThreshold && strideDuration > zeroThreshold){
                strideMaxSum += strideMax;
                strides.add(strideMax);
                strideDuration = 0;
                strideNum++;
                //Toast.makeText(this,"Stride max: "+String.format("Load: %.2f", strideMax),Toast.LENGTH_SHORT).show();
                Log.e(TAG, "renderFeedback: "+strideMax);
                if(audioOn) {
                    summarySound(strideMax);
                }
                strideMax = 0;
                updatePlot();
            }
        }else{
            strideDuration++;
        }
    }

    private void generateSound(double load, int indi) {
        float volume = 0f;
        double minGood = target-bodyWeight*successWidth;
        double maxGood = target+bodyWeight*successWidth;
        if(load <= minGood){
            mediaPlayerLow.setVolume(0,0);
            mediaPlayerHigh.setVolume(0,0);
        }
        if(load > minGood && load <= maxGood){
            volume = (float) 1;//(float)abs(1-(load/30));
            mediaPlayerLow.setVolume(volume,volume);
            mediaPlayerHigh.setVolume(0,0);
        }
        if(load > maxGood) {
            //volume = (float) (1 - (max - load) / (max - min));//(float)abs(1-(load/30));
            volume = 1;
            mediaPlayerHigh.setVolume(volume,volume);
            mediaPlayerLow.setVolume(0,0);
        }
    }

    private void summarySound(double load) {
        float volume;
        double minGood = target-bodyWeight*successWidth;
        double maxGood = target+bodyWeight*successWidth;
        if(load <= minGood){
            mediaPlayerLow.setVolume(0,0);
            mediaPlayerHigh.setVolume(0,0);
        }
        if(load > minGood && load <= maxGood){
            volume = (float) 1;//(float)abs(1-(load/30));
            mediaPlayerLow.setVolume(volume,volume);
            mediaPlayerHigh.setVolume(0,0);
        }
        if(load > maxGood) {
            //volume = (float) (1 - (max - load) / (max - min));//(float)abs(1-(load/30));
            volume = 1;
            mediaPlayerHigh.setVolume(volume,volume);
            mediaPlayerLow.setVolume(0,0);
        }

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mediaPlayerHigh.setVolume(0,0);
                mediaPlayerLow.setVolume(0,0);
            }
        }, 400L);
    }

    public void setFeedbackCondition(int cond){
        feedbackCondition = cond;
    }

    public int getFeedbackCondition(){
        return feedbackCondition;
    }

    public void resetStrides(){
        strideNum = 0;
        zeroDuration = 0;
        strideMax = 0;
        strideMaxSum = 0;
        strideDuration = 0;
        strides.removeAll(strides);
    }

    public void resetCondition(){
        frameCount = 0;
        strides.clear();

        mediaPlayerHigh.setVolume(0,0);
        mediaPlayerLow.setVolume(0,0);
    }

    public ArrayList<Double> getStrides() {
        return strides;
    }

    public double getBodyWeight(){
        return bodyWeight;
    }

    public void setBodyWeight(double newBodyWeight){
        bodyWeight = newBodyWeight;
        setTarget();
    }

    public void setTargetPercentBW(double newTarget){
        targetPercentBW = newTarget;
        setTarget();
    }

    public double getTargetPercentBW(){
        return targetPercentBW;
    }

    public double getTarget(){ return target; }

    public void setTarget(){ target = bodyWeight*targetPercentBW; }

}
