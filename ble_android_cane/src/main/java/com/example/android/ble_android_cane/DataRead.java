package com.example.android.ble_android_cane;

import android.util.Log;

import java.nio.ByteBuffer;
import java.util.Arrays;

import eu.darken.myolib.processor.emg.EmgData;

/**
 * Created by Ian on 2016-06-24.
 */
public class DataRead {

    private final static String TAG = DataRead.class.getSimpleName();

    public final char plusSign = '+';
    public static final char minusSign = '-';
    public static final int objLength = 16;    //TODO: Generalize this for cane & walker models
    public static final int pisonLength = 20;
    public static final int imuLength = 18;
    public static final int singleLength = 16;

    //public static CaneData translateData(byte[] input, long timeIn){
    public static CaneData translatePison(byte[] bytesIn, long timeIn){
        double[] pisonArr = new double[7];
        Arrays.fill(pisonArr,0);

        byte[] pisonTimeb = {bytesIn[0],bytesIn[1],bytesIn[2],bytesIn[3]};
        ByteBuffer buff;
        buff = ByteBuffer.wrap(pisonTimeb);
        double pisonTime = (double) buff.getInt();

        double[] quat = new double[4];
        byte[] temp = new byte[2];
        for(int i= 0; i < 8; i = i+2){
            temp[1] = bytesIn[i+pisonTimeb.length];
            temp[0] = bytesIn[i+1+pisonTimeb.length];
            buff = ByteBuffer.wrap(temp);
            quat[i/2] = (double) buff.getShort();
        }

        double[] emg = new double[2];
        byte[] temp2 = new byte[4];
        int offset = pisonTimeb.length+quat.length*2;   //4 bytes from time, 2x4 from quat, start at 12
        for(int j= 0; j< 8; j = j+4) {
            temp2[0] = bytesIn[j+offset];
            temp2[1] = bytesIn[j+1+offset];
            temp2[2] = bytesIn[j+2+offset];
            temp2[3] = bytesIn[j+3+offset];
            buff = ByteBuffer.wrap(temp2);
            emg[j/4] = (double) buff.getInt();
        }

        // android time, pison time, quatw, quatx, quaty, quatz, emg0, emg1, 0pad
        return new CaneData(timeIn, pisonTime, quat[0],quat[1],quat[2],quat[3],emg[0],emg[1],0,0,0,0,0,0,0,0,0,0);
    }

    public static CaneData translateData(byte[] input, long timeIn){
        double[] doubleArr = new double[(imuLength/2 + 8)];//sensorsIn.length)/2];
        Arrays.fill(doubleArr,0);
        byte[] temp;
        ByteBuffer buff;
        for(int i= 0; i < input.length; i = i+2){
            temp = new byte[2];
            temp[1] = input[i];
            temp[0] = input[i+1];
            buff = ByteBuffer.wrap(temp);
            doubleArr[i/2] = (double) buff.getShort();
        }
        doubleArr[8] = getLoad((int) doubleArr[6]); //Log calculated load as magz
        if(doubleArr[8] < FeedbackService.loadThreshold){
            doubleArr[8] = 0;
        }
        //android time, gyrox,gyroy,gyroz,accx,accy,accz,magx,magy,magz,sensor1,sensor2,sensor3,sensor4,sensor5,sensor6,sensor7,sensor8
        return new CaneData(timeIn, doubleArr[0],doubleArr[1],doubleArr[2],doubleArr[3], doubleArr[4],doubleArr[5],doubleArr[6],doubleArr[7],doubleArr[8],
                doubleArr[9],doubleArr[10],doubleArr[11], doubleArr[12],doubleArr[13],doubleArr[14],doubleArr[15],doubleArr[16]);
    }
    /*
    public static CaneData[] translateEmg(EmgData[] emgDataArr, long time){
        double[] doubleArr = new double[(imuLength/2 + 8)];//sensorsIn.length)/2];
        byte[] temp;
        Arrays.fill(doubleArr,0);
        ByteBuffer buff;
        byte[] data;
        CaneData[] outputArr = new CaneData[emgDataArr.length];
        EmgData emgData;

        for(int j = 0; j < emgDataArr.length; j++) {
            emgData = emgDataArr[j];
            data = emgData.getData();
            time++;

            for (int i = 0; i < data.length; i = i + 2) {
                temp = new byte[2];
                temp[1] = data[i];
                temp[0] = data[i + 1];
                buff = ByteBuffer.wrap(temp);
                doubleArr[i / 2] = (double) buff.getShort();
            }
            doubleArr[6] = (double) emgData.getTimestamp();
            outputArr[j] =  new CaneData(time, doubleArr[0],doubleArr[1],doubleArr[2],doubleArr[3], doubleArr[4],doubleArr[5],doubleArr[6],doubleArr[7],doubleArr[8],
                    doubleArr[9],doubleArr[10],doubleArr[11], doubleArr[12],doubleArr[13],doubleArr[14],doubleArr[15],doubleArr[16]);
        }
        return outputArr;
    }
    */

    public static double getLoad(int adc){
       /* double[] adcArr =  {11289,
                            10891,
                            10608,
                            10332,
                            9993,
                            9727,
                            9012,
                            8693,
                            8381,
                            8071,
                            7777,
                            7499,
                            7018,
                            6479,
                            6232,
                            5860,
                            5467};
        double[] massArr = {    0.0,
                                5.0,
                                10.0,
                                15.0,
                                20.0,
                                25.0,
                                30.0,
                                35.0,
                                40.0,
                                45.0,
                                50.0,
                                55.0,
                                60.0,
                                65.0,
                                70.0,
                                75.0,
                                80.0,
                                85.0,
                                90.0};
        if(adc >= adcArr[0]){
            return 0;
        }
        if(adc < adcArr[1]){
            Log.d(TAG, "getLoad: "+adc);
        }
        for(int i = 0; i < adcArr.length-1; i++){
            if(adc < adcArr[i] && adc >= adcArr[i+1]){
                return massArr[i]+( (adc-adcArr[i])/(adcArr[i+1]-adcArr[i]) )*(massArr[i+1]-massArr[i]);
            }
        }
        return ( (adc-adcArr[adcArr.length-1]) / (adcArr[adcArr.length-1]-adcArr[adcArr.length-1]) )*5 + massArr[massArr.length-1];
        */

       /*//Constants from after calibration shift, second half of caneScaleTile.h5
        double intercept = 127.7625;
        double slope = -0.0116096;*/

        //Constants from after calibration shift, for linear mapping load adc to kg scalePlank.h5
        double intercept = 73.058;
        double slope = -0.0067479;
        double caneLoad = ( (new Double(adc)) * slope) + intercept; //Load in kg
        caneLoad = caneLoad * 2.2046;   //Convert load to lbs
        if(caneLoad < 0){
            caneLoad = 0;
        }
        return caneLoad;

    }
}
