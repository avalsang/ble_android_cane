package com.example.android.ble_android_cane;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.File;
import java.util.LinkedList;
import java.util.Queue;

import eu.darken.myolib.processor.BaseDataPacket;
import eu.darken.myolib.processor.DataPacket;
import eu.darken.myolib.processor.emg.EmgData;
import eu.darken.myolib.processor.imu.ImuData;

/**
 * Created by Ian on 2016-05-10.
 */
public class DBHelper extends SQLiteOpenHelper {
    private static DBHelper sInstance;

    public static final String DATABASE_NAME = "Cane.db";
    public static final String CANE_TABLE_ANALYTICS = "caneAnalytics";    //Hourly averages
    public static final String CANE_TABLE_FULL = "caneFull";      //All raw data and angles
    public static final String CANE_TABLE_SHORT = "caneShort";      //All raw data and angles
    public static final String EMG_TABLE = "emgTable";      //All raw data and angles
    public static final String IMU_TABLE = "imuTable";      //All raw data and angles
    public static final String SENSOR_TABLE = "imuTable";      //All raw data and angles
    public static final String COLUMN_TIME = "time";
    public static final String COLUMN_ADDRESS = "address";
    public static final String COLUMN_ACC_X = "acc_x";
    public static final String COLUMN_ACC_Y = "acc_y";
    public static final String COLUMN_ACC_Z = "acc_z";
    public static final String COLUMN_GYRO_X = "gyro_x";
    public static final String COLUMN_GYRO_Y = "gyro_y";
    public static final String COLUMN_GYRO_Z = "gyro_z";
    public static final String COLUMN_MAG_X = "mag_x";
    public static final String COLUMN_MAG_Y = "mag_y";
    public static final String COLUMN_MAG_Z = "mag_z";
    public static final String COLUMN_EXTRA_1 = "extra_1";


    public static final String COLUMN_SENSOR_1 = "sensor1";
    public static final String COLUMN_SENSOR_2 = "sensor2";
    public static final String COLUMN_SENSOR_3 = "sensor3";
    public static final String COLUMN_SENSOR_4 = "sensor4";
    public static final String COLUMN_SENSOR_5 = "sensor5";
    public static final String COLUMN_SENSOR_6 = "sensor6";
    public static final String COLUMN_SENSOR_7 = "sensor7";
    public static final String COLUMN_SENSOR_8 = "sensor8";
    public static final String COLUMN_SENSOR_9 = "sensor9";
    public static final String COLUMN_SENSOR_10 = "sensor10";


    public static final String COLUMN_FORCE_VARIANCE = "forceVariance";
    public static final String COLUMN_PITCH_VARIANCE = "pitchVariance";
    public static final String COLUMN_ROLL_VARIANCE = "rollVariance";
    public static final String COLUMN_FORCE_MAX = "forceMax";
    public static final String COLUMN_ROLL_MEAN = "rollMean";
    public static final String TAG = "DBError";
    //private SQLiteDatabase db = null;

    private DBHelper(Context context, String databaseName) {
        //super(context, Environment.getExternalStorageDirectory().getAbsolutePath()
        //       + File.separator+DATABASE_NAME,null,1);
        super(context, databaseName, null, 1);
        //db = this.getReadableDatabase();
    }

    public static synchronized DBHelper getInstance(Context context) {
        // Use the application context, which will ensure that you
        // don't accidentally leak an Activity's context.
        // See this article for more information: http://bit.ly/6LRzfx
        if (sInstance == null) {
            /*File[] files = context.getExternalFilesDirs(null);
            File f = new File(files[files.length-1],DATABASE_NAME);*/
            String dbPath = context.getDatabasePath("Placeholder").getParentFile().getPath();
            sInstance = new DBHelper(context.getApplicationContext(), dbPath+File.separator+DATABASE_NAME);
        }
        return sInstance;
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS " + CANE_TABLE_ANALYTICS + "(time long primary key, forceMax double, rollMean double, forceVariance double," +
                "pitchVariance double, rollVariance double)");
        db.execSQL("CREATE TABLE IF NOT EXISTS " + CANE_TABLE_FULL + "(primKey INTEGER primary key AUTOINCREMENT, time long, gyro_x double, gyro_y double, gyro_z double, " +
                "acc_x double, acc_y double, acc_z double, mag_x double, mag_y double, mag_z double, sensor1 double, sensor2 double,"+
                "sensor3 double, sensor4 double, sensor5 double, sensor6 double, sensor7 double, sensor8 double)");

        db.execSQL("CREATE TABLE IF NOT EXISTS " + CANE_TABLE_SHORT + "(primKey INTEGER primary key AUTOINCREMENT, time long, sensor3 double, acc_x double, acc_y double, acc_z double," +
                "gyro_x double, gyro_y double, gyro_z double, sensor1 double, sensor2 double, sensor4 double)");

        db.execSQL("CREATE TABLE IF NOT EXISTS " + EMG_TABLE + "(primKey INTEGER primary key AUTOINCREMENT, address String, time long, sensor1 short, sensor2 short, sensor3 short," +
                "sensor4 short, sensor5 short, sensor6 short, sensor7 short, sensor8 short)");
        db.execSQL("CREATE TABLE IF NOT EXISTS " + IMU_TABLE + "(primKey INTEGER primary key AUTOINCREMENT, address String, time long, acc_x double, acc_y double, acc_z double," +
                "gyro_x double, gyro_y double, gyro_z double, mag_x double, mag_y double, mag_z double, extra_1 double)");
        db.execSQL("CREATE TABLE IF NOT EXISTS " + SENSOR_TABLE + "(primKey INTEGER primary key AUTOINCREMENT, address String, time long, sensor1 short, sensor2 short, sensor3 short," +
                "sensor4 short, sensor5 short, sensor6 short, sensor7 short, sensor8 short)");
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + CANE_TABLE_ANALYTICS);
        db.execSQL("DROP TABLE IF EXISTS " + CANE_TABLE_FULL);
        db.execSQL("DROP TABLE IF EXISTS " + CANE_TABLE_SHORT);

        db.execSQL("DROP TABLE IF EXISTS " + EMG_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + IMU_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + SENSOR_TABLE);
        onCreate(db);
    }

    public void resetTable() { //Tested and functional
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DROP TABLE IF EXISTS " + CANE_TABLE_ANALYTICS);
        /*db.execSQL("CREATE TABLE IF NOT EXISTS " + CANE_TABLE_ANALYTICS + "(time long primary key, forceMax double, rollMean double, forceVariance double," +
                "pitchVariance double, rollVariance double)");*/
        db.execSQL("DROP TABLE IF EXISTS " + CANE_TABLE_FULL);
        /*db.execSQL("CREATE TABLE IF NOT EXISTS " + CANE_TABLE_FULL + "(time long primary key, sensor3 double, acc_x double, acc_y double, acc_z double," +
                "gyro_x double, gyro_y double, gyro_z double, sensor1 double, sensor2 double)");*/
        db.execSQL("DROP TABLE IF EXISTS " + CANE_TABLE_SHORT);

        db.execSQL("DROP TABLE IF EXISTS " + EMG_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + IMU_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + SENSOR_TABLE);
        onCreate(db);
    }

    /*
    *
    *   Cane section
    *
     */
    public static void deleteCaneRows(long starttime, long endtime){
        SQLiteDatabase db = sInstance.getWritableDatabase();
        db.beginTransaction();
        try {
            db.execSQL("DELETE FROM " + CANE_TABLE_FULL + " WHERE time >= " + Long.toString(starttime) +
                    " AND time <= " + Long.toString(endtime));

            db.setTransactionSuccessful();
        }catch(Exception e){
            Log.d(TAG,e.getMessage());
        }finally{
            db.endTransaction();
        }
    }

    public int getCaneTableCount() //Tested and functional, but not tested to limits of full DB
    {
        int rowNum = 0;
        SQLiteDatabase db = this.getReadableDatabase();

        synchronized (this) {
            db.beginTransaction();
            Cursor res = null;
            try {

                res = db.rawQuery("select COUNT(time) from " + CANE_TABLE_FULL, null);
                res.moveToFirst();
                rowNum = res.getInt(0);
                db.setTransactionSuccessful();
            } catch (Exception e) {
            } finally {
                if (res != null) {
                    res.close();
                }
                db.endTransaction();
                //db.close();
                return rowNum;
            }
        }
    }
    //bulk insert raw data
    public static String caneInsertLoop(CaneData[] caneArr) { //Inserts values for all available columns
        SQLiteDatabase db = sInstance.getWritableDatabase();
        String error = "Clear";
        int failure = 0;
        db.beginTransaction();
        try {
            for (int i = 0; i < caneArr.length; i++) {
                if (caneArr[i] == null) {
                    continue;
                }
                //int idIn = (int) (caneArr[i].time % 1000000000L);
                ContentValues values = new ContentValues();
                values.put(COLUMN_TIME, caneArr[i].time);
                values.put(COLUMN_ACC_X, caneArr[i].accx);
                values.put(COLUMN_ACC_Y, caneArr[i].accy);
                values.put(COLUMN_ACC_Z, caneArr[i].accz);
                values.put(COLUMN_GYRO_X, caneArr[i].gyrox);
                values.put(COLUMN_GYRO_Y, caneArr[i].gyroy);
                values.put(COLUMN_GYRO_Z, caneArr[i].gyroz);
                values.put(COLUMN_MAG_X, caneArr[i].magx);
                values.put(COLUMN_MAG_Y, caneArr[i].magy);
                values.put(COLUMN_MAG_Z, caneArr[i].magz);
                values.put(COLUMN_SENSOR_1, caneArr[i].sensor1);
                values.put(COLUMN_SENSOR_2, caneArr[i].sensor2);
                values.put(COLUMN_SENSOR_3, caneArr[i].sensor3);
                values.put(COLUMN_SENSOR_4, caneArr[i].sensor4);
                values.put(COLUMN_SENSOR_5, caneArr[i].sensor5);
                values.put(COLUMN_SENSOR_6, caneArr[i].sensor6);
                values.put(COLUMN_SENSOR_7, caneArr[i].sensor7);
                values.put(COLUMN_SENSOR_8, caneArr[i].sensor8);

                db.insertOrThrow(CANE_TABLE_FULL, null, values);
            }
            db.setTransactionSuccessful();
        } catch (Exception e) {
            //db.endTransaction();
            failure = 1;
            error = e.getMessage();
            Log.d(TAG, "caneInsertLoop: "+error);
        } finally {
            db.endTransaction();
            //db.close();
            }
            return error;
        }

    //Pull chunks of raw data
    public CaneData[] getCaneHour(long hour, long interval) { //Tested and functional
        long min = hour;
        long max = hour + interval;
        SQLiteDatabase db = this.getReadableDatabase();
        CaneData[] caneArr = null;
        synchronized (this) {
            db.beginTransaction();
            Cursor res = null;
            try {
                res = db.rawQuery("select * from " + CANE_TABLE_FULL + " WHERE time >= " + Long.toString(min)
                        + " AND time < " + Long.toString(max), null);
                res.moveToFirst();
                caneArr = new CaneData[res.getCount()];
                int rowNum = 0;

                while (res.isAfterLast() == false) {
                    long time = Long.parseLong(res.getString(res.getColumnIndex(COLUMN_TIME)));

                    double accx = Double.parseDouble(res.getString(res.getColumnIndex(COLUMN_ACC_X)));
                    double accy = Double.parseDouble(res.getString(res.getColumnIndex(COLUMN_ACC_Y)));
                    double accz = Double.parseDouble(res.getString(res.getColumnIndex(COLUMN_ACC_Z)));
                    double gyrox = Double.parseDouble(res.getString(res.getColumnIndex(COLUMN_GYRO_X)));
                    double gyroy = Double.parseDouble(res.getString(res.getColumnIndex(COLUMN_GYRO_Y)));
                    double gyroz = Double.parseDouble(res.getString(res.getColumnIndex(COLUMN_GYRO_Z)));
                    double magx = Double.parseDouble(res.getString(res.getColumnIndex(COLUMN_MAG_X)));
                    double magy = Double.parseDouble(res.getString(res.getColumnIndex(COLUMN_MAG_Y)));
                    double magz = Double.parseDouble(res.getString(res.getColumnIndex(COLUMN_MAG_Z)));
                    double sensor1 = Double.parseDouble(res.getString(res.getColumnIndex(COLUMN_SENSOR_1)));
                    double sensor2 = Double.parseDouble(res.getString(res.getColumnIndex(COLUMN_SENSOR_2)));
                    double sensor3 = Double.parseDouble(res.getString(res.getColumnIndex(COLUMN_SENSOR_3)));
                    double sensor4 = Double.parseDouble(res.getString(res.getColumnIndex(COLUMN_SENSOR_4)));
                    double sensor5 = Double.parseDouble(res.getString(res.getColumnIndex(COLUMN_SENSOR_5)));
                    double sensor6 = Double.parseDouble(res.getString(res.getColumnIndex(COLUMN_SENSOR_6)));
                    double sensor7 = Double.parseDouble(res.getString(res.getColumnIndex(COLUMN_SENSOR_7)));
                    double sensor8 = Double.parseDouble(res.getString(res.getColumnIndex(COLUMN_SENSOR_8)));
                    caneArr[rowNum] = new CaneData(time, gyrox, gyroy, gyroz, accx, accy, accz, magx, magy, magz,
                                            sensor1, sensor2, sensor3, sensor4, sensor5, sensor6, sensor7, sensor8);
                    rowNum++;
                    res.moveToNext();
                }
                db.setTransactionSuccessful();
            } catch (Exception e) {
            } finally {
                if (res != null) {
                    res.close();
                }
                db.endTransaction();
                return caneArr;
            }
        }
    }


    //Returns the minimum and maximum time values stored in the raw DB
    public long[] getCaneMinAndMaxTime() {
        SQLiteDatabase db = this.getReadableDatabase();
        long timeMin = -1;
        long timeMax = -1;
        Cursor res = null;

        db.beginTransaction();
        try {
            res = db.rawQuery("select MIN(time) from " + CANE_TABLE_FULL, null);
            res.moveToFirst();
            while (res.isAfterLast() == false) {
                String testMin = res.getString(0);
                timeMin = res.getLong(0);
                res.moveToNext();
                //timeMin = Long.parseLong(res.getString(res.getColumnIndex("MIN(time)")));
            }
            res.close();
            res = db.rawQuery("select MAX(time) from " + CANE_TABLE_FULL, null);
            res.moveToFirst();
            while (res.isAfterLast() == false) {
                timeMax = res.getLong(0);
                res.moveToNext();
            }
            db.setTransactionSuccessful();

        } catch (Exception e) {
            int failure = 1;
            String error = e.getMessage();
            Log.d(TAG,error);

        } finally {
            db.endTransaction();
            if(res != null) {
                res.close();
            }
            //db.close();
        }
        return new long[]{timeMin, timeMax};
    }

    /*
    *
    * EMG Section
    *
     */

    public static void deleteEmgRows(long starttime, long endtime){
        SQLiteDatabase db = sInstance.getWritableDatabase();
        db.beginTransaction();
        try {
            db.execSQL("DELETE FROM " + EMG_TABLE + " WHERE time >= " + Long.toString(starttime) +
                    " AND time <= " + Long.toString(endtime));

            db.setTransactionSuccessful();
        }catch(Exception e){
            Log.d(TAG,e.getMessage());
        }finally{
            db.endTransaction();
        }
    }

    public int getEmgTableCount() //Tested and functional, but not tested to limits of full DB
    {
        int rowNum = 0;
        SQLiteDatabase db = this.getReadableDatabase();

        synchronized (this) {
            db.beginTransaction();
            Cursor res = null;
            try {

                res = db.rawQuery("select COUNT(time) from " + EMG_TABLE, null);
                res.moveToFirst();
                rowNum = res.getInt(0);
                db.setTransactionSuccessful();
            } catch (Exception e) {
            } finally {
                if (res != null) {
                    res.close();
                }
                db.endTransaction();
                return rowNum;
            }
        }
    }
    //bulk insert raw data
    public static String emgInsertLoop(EmgData[] dataArr) { //Inserts values for all available columns
        SQLiteDatabase db = sInstance.getWritableDatabase();
        String error = "Clear";
        int failure = 0;
        db.beginTransaction();
        try {
            for (int i = 0; i < dataArr.length; i++) {
                if (dataArr[i] == null) {
                    continue;
                }
                byte[] data = dataArr[i].getData();
                ContentValues values = new ContentValues();
                values.put(COLUMN_TIME, dataArr[i].getTimestamp());
                values.put(COLUMN_ADDRESS, dataArr[i].getDeviceAddress());
                values.put(COLUMN_SENSOR_1, data[0]);
                values.put(COLUMN_SENSOR_2, data[1]);
                values.put(COLUMN_SENSOR_3, data[2]);
                values.put(COLUMN_SENSOR_4, data[3]);
                values.put(COLUMN_SENSOR_5, data[4]);
                values.put(COLUMN_SENSOR_6, data[5]);
                values.put(COLUMN_SENSOR_7, data[6]);
                values.put(COLUMN_SENSOR_8, data[7]);

                db.insertOrThrow(EMG_TABLE, null, values);
            }
            db.setTransactionSuccessful();
        } catch (Exception e) {
            failure = 1;
            error = e.getMessage();
        } finally {
            db.endTransaction();
        }
        return error;
    }

    //Pull chunks of raw data
    public EmgData[] getEmgHour(long hour, long interval) { //Tested and functional
        long min = hour;
        long max = hour + interval;
        SQLiteDatabase db = this.getReadableDatabase();
        EmgData[] dataArr = null;
        synchronized (this) {
            db.beginTransaction();
            Cursor res = null;
            try {
                res = db.rawQuery("select * from " + EMG_TABLE + " WHERE time >= " + Long.toString(min)
                        + " AND time < " + Long.toString(max), null);
                res.moveToFirst();
                dataArr = new EmgData[res.getCount()];
                byte[] bytes = new byte[8];
                int rowNum = 0;

                while (res.isAfterLast() == false) {
                    long time = Long.parseLong(res.getString(res.getColumnIndex(COLUMN_TIME)));
                    String address = res.getString(res.getColumnIndex(COLUMN_ADDRESS));

                    bytes[1] = Byte.parseByte(res.getString(res.getColumnIndex(COLUMN_SENSOR_1)));
                    bytes[2] = Byte.parseByte(res.getString(res.getColumnIndex(COLUMN_SENSOR_2)));
                    bytes[3] = Byte.parseByte(res.getString(res.getColumnIndex(COLUMN_SENSOR_3)));
                    bytes[4] = Byte.parseByte(res.getString(res.getColumnIndex(COLUMN_SENSOR_4)));
                    bytes[5] = Byte.parseByte(res.getString(res.getColumnIndex(COLUMN_SENSOR_5)));
                    bytes[6] = Byte.parseByte(res.getString(res.getColumnIndex(COLUMN_SENSOR_6)));
                    bytes[7] = Byte.parseByte(res.getString(res.getColumnIndex(COLUMN_SENSOR_7)));
                    bytes[8] = Byte.parseByte(res.getString(res.getColumnIndex(COLUMN_SENSOR_8)));
                    dataArr[rowNum] = new EmgData(address, time, bytes);
                    rowNum++;
                    res.moveToNext();
                }
                db.setTransactionSuccessful();
            } catch (Exception e) {
            } finally {
                if (res != null) {
                    res.close();
                }
                db.endTransaction();
                return dataArr;
            }
        }
    }


    //Returns the minimum and maximum time values stored in the raw DB
    public long[] getEmgMinAndMaxTime() {
        SQLiteDatabase db = this.getReadableDatabase();
        long timeMin = -1;
        long timeMax = -1;
        Cursor res = null;

        db.beginTransaction();
        try {
            res = db.rawQuery("select MIN(time) from " + EMG_TABLE, null);
            res.moveToFirst();
            while (res.isAfterLast() == false) {
                String testMin = res.getString(0);
                timeMin = res.getLong(0);
                res.moveToNext();
            }
            res.close();
            res = db.rawQuery("select MAX(time) from " + EMG_TABLE, null);
            res.moveToFirst();
            while (res.isAfterLast() == false) {
                timeMax = res.getLong(0);
                res.moveToNext();
            }
            db.setTransactionSuccessful();

        } catch (Exception e) {
            int failure = 1;
            String error = e.getMessage();
            Log.d(TAG,error);

        } finally {
            db.endTransaction();
            if(res != null) {
                res.close();
            }
            //db.close();
        }
        return new long[]{timeMin, timeMax};
    }

    /*
     *
     * IMU Section
     *
     */

    public static void deleteImuRows(long starttime, long endtime){
        SQLiteDatabase db = sInstance.getWritableDatabase();
        db.beginTransaction();
        try {
            db.execSQL("DELETE FROM " + IMU_TABLE + " WHERE time >= " + Long.toString(starttime) +
                    " AND time <= " + Long.toString(endtime));

            db.setTransactionSuccessful();
        }catch(Exception e){
            Log.d(TAG,e.getMessage());
        }finally{
            db.endTransaction();
        }
    }

    public int getImuTableCount() //Tested and functional, but not tested to limits of full DB
    {
        int rowNum = 0;
        SQLiteDatabase db = this.getReadableDatabase();

        synchronized (this) {
            db.beginTransaction();
            Cursor res = null;
            try {

                res = db.rawQuery("select COUNT(time) from " + IMU_TABLE, null);
                res.moveToFirst();
                rowNum = res.getInt(0);
                db.setTransactionSuccessful();
            } catch (Exception e) {
            } finally {
                if (res != null) {
                    res.close();
                }
                db.endTransaction();
                //db.close();
                return rowNum;
            }
        }
    }
    //bulk insert raw data
    public static String imuInsertLoop(ImuData[] imuArr) { //Inserts values for all available columns
        SQLiteDatabase db = sInstance.getWritableDatabase();
        String error = "Clear";
        int failure = 0;
        db.beginTransaction();
        double[] acc;
        double[] gyro;
        double[] orient;
        try {
            for (int i = 0; i < imuArr.length; i++) {
                if (imuArr[i] == null) {
                    continue;
                }
                acc = imuArr[i].getAccelerometerData();
                gyro = imuArr[i].getGyroData();
                orient = imuArr[i].getOrientationData();

                ContentValues values = new ContentValues();
                values.put(COLUMN_TIME, imuArr[i].getTimeStamp());
                values.put(COLUMN_ADDRESS, imuArr[i].getDeviceAddress());
                values.put(COLUMN_ACC_X, acc[0]);
                values.put(COLUMN_ACC_Y, acc[1]);
                values.put(COLUMN_ACC_Z, acc[2]);
                values.put(COLUMN_GYRO_X, gyro[0]);
                values.put(COLUMN_GYRO_Y, gyro[1]);
                values.put(COLUMN_GYRO_Z, gyro[2]);
                values.put(COLUMN_MAG_X, orient[0]);
                values.put(COLUMN_MAG_Y, orient[1]);
                values.put(COLUMN_MAG_Z, orient[2]);
                values.put(COLUMN_EXTRA_1, orient[3]);

                db.insertOrThrow(IMU_TABLE, null, values);
            }
            db.setTransactionSuccessful();
        } catch (Exception e) {
            error = e.getMessage();
        } finally {
            db.endTransaction();
        }
        return error;
    }

    //Pull chunks of raw data
    public ImuData[] getImuHour(long hour, long interval) { //Tested and functional
        long min = hour;
        long max = hour + interval;
        SQLiteDatabase db = this.getReadableDatabase();
        ImuData[] dataArr = null;
        synchronized (this) {
            db.beginTransaction();
            Cursor res = null;
            try {
                res = db.rawQuery("select * from " + IMU_TABLE + " WHERE time >= " + Long.toString(min)
                        + " AND time < " + Long.toString(max), null);
                res.moveToFirst();
                dataArr = new ImuData[res.getCount()];
                int rowNum = 0;

                while (res.isAfterLast() == false) {
                    byte[] bytes = new byte[10];
                    long time = Long.parseLong(res.getString(res.getColumnIndex(COLUMN_TIME)));

                    String address = res.getString(res.getColumnIndex(COLUMN_ADDRESS));

                    bytes[0] = Byte.parseByte(res.getString(res.getColumnIndex(COLUMN_MAG_X)));
                    bytes[1] = Byte.parseByte(res.getString(res.getColumnIndex(COLUMN_MAG_Y)));
                    bytes[2] = Byte.parseByte(res.getString(res.getColumnIndex(COLUMN_MAG_Z)));
                    bytes[3] = Byte.parseByte(res.getString(res.getColumnIndex(COLUMN_EXTRA_1)));
                    bytes[4] = Byte.parseByte(res.getString(res.getColumnIndex(COLUMN_ACC_X)));
                    bytes[5] = Byte.parseByte(res.getString(res.getColumnIndex(COLUMN_ACC_Y)));
                    bytes[6] = Byte.parseByte(res.getString(res.getColumnIndex(COLUMN_ACC_Z)));
                    bytes[7] = Byte.parseByte(res.getString(res.getColumnIndex(COLUMN_GYRO_X)));
                    bytes[8] = Byte.parseByte(res.getString(res.getColumnIndex(COLUMN_GYRO_Y)));
                    bytes[9] = Byte.parseByte(res.getString(res.getColumnIndex(COLUMN_GYRO_Z)));

                    //TODO: include manual time set
                    BaseDataPacket base = new BaseDataPacket(address, null, null, bytes);
                    dataArr[rowNum] = new ImuData(base);

                    rowNum++;
                    res.moveToNext();
                }
                db.setTransactionSuccessful();
            } catch (Exception e) {
            } finally {
                if (res != null) {
                    res.close();
                }
                db.endTransaction();
                return dataArr;
            }
        }
    }


    //Returns the minimum and maximum time values stored in the raw DB
    public long[] getImuMinAndMaxTime() {
        SQLiteDatabase db = this.getReadableDatabase();
        long timeMin = -1;
        long timeMax = -1;
        Cursor res = null;

        db.beginTransaction();
        try {
            res = db.rawQuery("select MIN(time) from " + IMU_TABLE, null);
            res.moveToFirst();
            while (res.isAfterLast() == false) {
                String testMin = res.getString(0);
                timeMin = res.getLong(0);
                res.moveToNext();
            }
            res.close();
            res = db.rawQuery("select MAX(time) from " + IMU_TABLE, null);
            res.moveToFirst();
            while (res.isAfterLast() == false) {
                timeMax = res.getLong(0);
                res.moveToNext();
            }
            db.setTransactionSuccessful();

        } catch (Exception e) {
            String error = e.getMessage();
            Log.d(TAG,error);

        } finally {
            db.endTransaction();
            if(res != null) {
                res.close();
            }
        }
        return new long[]{timeMin, timeMax};
    }

    /*
     *
     * Generic Sensors Section
     *
     */

    /*
    *
    * Plotting/retrieval section
    *
     */

    //Generalized method for pulling chunks of raw data
    public CaneData[] getInterval(long windowMin, long intervalSize, long modulator) {
        Cursor res = null;
        CaneData[] caneArr = null;
        SQLiteDatabase db = this.getReadableDatabase();
        try{
            res = db.rawQuery("select * from " + CANE_TABLE_FULL + " WHERE time >= " + Long.toString(windowMin)
                    + " AND time < " + Long.toString(windowMin + intervalSize) + " AND time % " + Long.toString(modulator)
                    + " = 0", null);
            res.moveToFirst();
            caneArr = new CaneData[res.getCount()];
            int rowNum = 0;
            db.beginTransaction();
            long time;

            double accx;
            double accy;
            double accz;
            double gyrox;
            double gyroy;
            double gyroz;
            double magx;
            double magy;
            double magz;
            double sensor1;
            double sensor2;
            double sensor3;
            double sensor4;
            double sensor5;
            double sensor6;
            double sensor7;
            double sensor8;
            while (res.isAfterLast() == false) {
                time = Long.parseLong(res.getString(res.getColumnIndex(COLUMN_TIME)));

                accx = Double.parseDouble(res.getString(res.getColumnIndex(COLUMN_ACC_X)));
                accy = Double.parseDouble(res.getString(res.getColumnIndex(COLUMN_ACC_Y)));
                accz = Double.parseDouble(res.getString(res.getColumnIndex(COLUMN_ACC_Z)));
                gyrox = Double.parseDouble(res.getString(res.getColumnIndex(COLUMN_GYRO_X)));
                gyroy = Double.parseDouble(res.getString(res.getColumnIndex(COLUMN_GYRO_Y)));
                gyroz = Double.parseDouble(res.getString(res.getColumnIndex(COLUMN_GYRO_Z)));
                magx = Double.parseDouble(res.getString(res.getColumnIndex(COLUMN_MAG_X)));
                magy = Double.parseDouble(res.getString(res.getColumnIndex(COLUMN_MAG_Y)));
                magz = Double.parseDouble(res.getString(res.getColumnIndex(COLUMN_MAG_Z)));
                sensor1 = Double.parseDouble(res.getString(res.getColumnIndex(COLUMN_SENSOR_1)));
                sensor2 = Double.parseDouble(res.getString(res.getColumnIndex(COLUMN_SENSOR_2)));
                sensor3 = Double.parseDouble(res.getString(res.getColumnIndex(COLUMN_SENSOR_3)));
                sensor4 = Double.parseDouble(res.getString(res.getColumnIndex(COLUMN_SENSOR_4)));
                sensor5 = Double.parseDouble(res.getString(res.getColumnIndex(COLUMN_SENSOR_5)));
                sensor6 = Double.parseDouble(res.getString(res.getColumnIndex(COLUMN_SENSOR_6)));
                sensor7 = Double.parseDouble(res.getString(res.getColumnIndex(COLUMN_SENSOR_7)));
                sensor8 = Double.parseDouble(res.getString(res.getColumnIndex(COLUMN_SENSOR_8)));
                caneArr[rowNum] = new CaneData(time, gyrox, gyroy, gyroz, accx, accy, accz, magx, magy, magz, sensor1, sensor2, sensor3, sensor4, sensor5, sensor6, sensor7, sensor8);
                rowNum++;
                res.moveToNext();
            }
        } catch (Exception e) {
            int failure = 1;
            String error = e.getMessage();
            Log.d(TAG, error);
        } finally {
            if(res != null){
                res.close();
            }
            db.endTransaction();
            //db.close();
        }
        return caneArr;
    }

    //TODO: Address modulator. Fix this mess
    public CaneData[] plotUpdate(long min, long max, long modulator) {
        long maxPull = (max / modulator) * modulator + 10 * modulator;
        long minPull = (min / modulator) * modulator - 10 * modulator;

        SQLiteDatabase db = this.getReadableDatabase();

        try {
            long minOut = -1;
            long maxOut = -1;
            db.beginTransaction();

            synchronized (this) {
                //Get boundaries of DB call
                Cursor resMin = db.rawQuery("select MIN(time) from " + CANE_TABLE_FULL + " WHERE time >= " + Long.toString(minPull)
                        + " AND time <= " + Long.toString(maxPull), null);
                resMin.moveToFirst();
                while (resMin.isAfterLast() == false) {
                    String testMin = resMin.getString(0);
                    minOut = resMin.getLong(0);
                    resMin.moveToNext();
                }
                resMin.close();
                Cursor resMax = db.rawQuery("select MAX(time) from " + CANE_TABLE_FULL + " WHERE time >= " + Long.toString(minPull)
                        + " AND time <= " + Long.toString(maxPull), null);
                resMax.moveToFirst();

                while (resMax.isAfterLast() == false) {
                    String testMin = resMax.getString(0);
                    maxOut = resMax.getLong(0);
                    resMax.moveToNext();
                }
                resMax.close();
            }

            CaneData[] caneArr = modVals(minOut,maxOut,modulator);
            db.setTransactionSuccessful();
            return caneArr;
        } catch (Exception e) {
            //e.getMessage();
            return null;
        }finally{
            db.endTransaction();
            //db.close();
        }
    }

    public CaneData[] modVals(long minOut, long maxOut, long mod){
        SQLiteDatabase db = this.getReadableDatabase();
        long span = maxOut-minOut;
        int count = (int)(span/mod);
        if((span % mod) != 0){
            count++;
        }
        Queue<CaneData> queue = new LinkedList<>();
        int listLength = 0;

        synchronized (this) {
            Cursor res = null;
            for (int i = 0; i < count; i++) {
                db.beginTransaction();
                try {
                    res = db.rawQuery("select * from " + CANE_TABLE_FULL + " WHERE time >= " + Long.toString(minOut + i * mod)
                            + " AND time < " + Long.toString(minOut + (i + 1) * mod), null);
                    res.moveToFirst();
                    if (res.isAfterLast()) {
                        continue;
                    }
                    String temper = res.toString();
                    int tempy = res.getColumnIndex(COLUMN_TIME);
                    long time = Long.parseLong(res.getString(res.getColumnIndex(COLUMN_TIME)));

                    double accx = Double.parseDouble(res.getString(res.getColumnIndex(COLUMN_ACC_X)));
                    double accy = Double.parseDouble(res.getString(res.getColumnIndex(COLUMN_ACC_Y)));
                    double accz = Double.parseDouble(res.getString(res.getColumnIndex(COLUMN_ACC_Z)));
                    double gyrox = Double.parseDouble(res.getString(res.getColumnIndex(COLUMN_GYRO_X)));
                    double gyroy = Double.parseDouble(res.getString(res.getColumnIndex(COLUMN_GYRO_Y)));
                    double gyroz = Double.parseDouble(res.getString(res.getColumnIndex(COLUMN_GYRO_Z)));
                    double magx = Double.parseDouble(res.getString(res.getColumnIndex(COLUMN_MAG_X)));
                    double magy = Double.parseDouble(res.getString(res.getColumnIndex(COLUMN_MAG_Y)));
                    double magz = Double.parseDouble(res.getString(res.getColumnIndex(COLUMN_MAG_Z)));
                    double sensor1 = Double.parseDouble(res.getString(res.getColumnIndex(COLUMN_SENSOR_1)));
                    double sensor2 = Double.parseDouble(res.getString(res.getColumnIndex(COLUMN_SENSOR_2)));
                    double sensor3 = Double.parseDouble(res.getString(res.getColumnIndex(COLUMN_SENSOR_3)));
                    double sensor4 = Double.parseDouble(res.getString(res.getColumnIndex(COLUMN_SENSOR_4)));
                    double sensor5 = Double.parseDouble(res.getString(res.getColumnIndex(COLUMN_SENSOR_5)));
                    double sensor6 = Double.parseDouble(res.getString(res.getColumnIndex(COLUMN_SENSOR_6)));
                    double sensor7 = Double.parseDouble(res.getString(res.getColumnIndex(COLUMN_SENSOR_7)));
                    double sensor8 = Double.parseDouble(res.getString(res.getColumnIndex(COLUMN_SENSOR_8)));
                    queue.add(new CaneData(time, gyrox, gyroy, gyroz, accx, accy, accz, magx, magy, magz, sensor1,
                            sensor2, sensor3, sensor4, sensor5, sensor6, sensor7, sensor8));
                    listLength++;
                    db.setTransactionSuccessful();
                } catch (Exception e) {
                    Log.d(TAG, e.getMessage());
                    //caneArr[i] = new CaneData(minOut+i*mod, 0,0,0,0,0,0,0,0,0); //null;
                } finally {
                    db.endTransaction();
                    if (res != null) {
                        res.close();
                    }
                }
            }
        }
        if(listLength > 0){
            CaneData[] caneArr = new CaneData[listLength];
            int ind = 0;
            for(CaneData caner : queue){
                caneArr[ind] = caner;
                ind++;
            }
            return caneArr;
        }else{
            return null;
        }
    }

    public CaneData[] averageVals(long minOut, long maxOut, long mod){
        SQLiteDatabase db = this.getReadableDatabase();

        long span = maxOut-minOut;
        int count = (int)(span/mod);
        if((span % mod) != 0){
            count++;
        }
        CaneData[] caneArr = new CaneData[count];

        synchronized (this) {
            Cursor res = null;
            for (int i = 0; i < count; i++) {
                db.beginTransaction();
                try {
                    int rowNum = 0;
                    res = db.rawQuery("select * from " + CANE_TABLE_FULL + " WHERE time >= " + Long.toString(minOut + i * mod)
                            + " AND time < " + Long.toString(minOut + (i + 1) * mod), null);
                    res.moveToFirst();
                    long time = Long.parseLong(res.getString(res.getColumnIndex(COLUMN_TIME)));
                    double accx = 0;
                    double accy = 0;
                    double accz = 0;
                    double gyrox = 0;
                    double gyroy = 0;
                    double gyroz = 0;
                    double magx = 0;
                    double magy = 0;
                    double magz = 0;
                    double sensor1 = 0;
                    double sensor2 = 0;
                    double sensor3 = 0;
                    double sensor4 = 0;
                    double sensor5 = 0;
                    double sensor6 = 0;
                    double sensor7 = 0;
                    double sensor8 = 0;
                    while (res.isAfterLast() == false) {
                        accx += Double.parseDouble(res.getString(res.getColumnIndex(COLUMN_ACC_X)));
                        accy += Double.parseDouble(res.getString(res.getColumnIndex(COLUMN_ACC_Y)));
                        accz += Double.parseDouble(res.getString(res.getColumnIndex(COLUMN_ACC_Z)));
                        gyrox += Double.parseDouble(res.getString(res.getColumnIndex(COLUMN_GYRO_X)));
                        gyroy += Double.parseDouble(res.getString(res.getColumnIndex(COLUMN_GYRO_Y)));
                        gyroz += Double.parseDouble(res.getString(res.getColumnIndex(COLUMN_GYRO_Z)));
                        magx += Double.parseDouble(res.getString(res.getColumnIndex(COLUMN_MAG_X)));
                        magy += Double.parseDouble(res.getString(res.getColumnIndex(COLUMN_MAG_Y)));
                        magz += Double.parseDouble(res.getString(res.getColumnIndex(COLUMN_MAG_Z)));
                        sensor1 += Double.parseDouble(res.getString(res.getColumnIndex(COLUMN_SENSOR_1)));
                        sensor2 += Double.parseDouble(res.getString(res.getColumnIndex(COLUMN_SENSOR_2)));
                        sensor3 += Double.parseDouble(res.getString(res.getColumnIndex(COLUMN_SENSOR_3)));
                        sensor4 += Double.parseDouble(res.getString(res.getColumnIndex(COLUMN_SENSOR_4)));
                        sensor5 += Double.parseDouble(res.getString(res.getColumnIndex(COLUMN_SENSOR_5)));
                        sensor6 += Double.parseDouble(res.getString(res.getColumnIndex(COLUMN_SENSOR_6)));
                        sensor7 += Double.parseDouble(res.getString(res.getColumnIndex(COLUMN_SENSOR_7)));
                        sensor8 += Double.parseDouble(res.getString(res.getColumnIndex(COLUMN_SENSOR_8)));
                        //caneArr[rowNum] = new CaneData(time, accx, accy, accz, gyrox, gyroy, gyroz, sensor1, sensor2, sensor3);
                        rowNum++;
                        res.moveToNext();
                    }
                    //res.close();
                    caneArr[i] = new CaneData(time,
                            gyrox / rowNum, gyroy / rowNum, gyroz / rowNum,accx / rowNum, accy / rowNum, accz / rowNum,magx / rowNum, magy / rowNum, magz / rowNum, sensor1 / rowNum,
                            sensor2 / rowNum, sensor3 / rowNum, sensor4/rowNum, sensor5 / rowNum, sensor6 / rowNum, sensor7 / rowNum, sensor8/rowNum);
                    db.setTransactionSuccessful();
                } catch (Exception e) {
                    Log.d(TAG, e.getMessage());
                    caneArr[i] = new CaneData(minOut + i * mod, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                            0, 0, 0, 0, 0, 0, 0, 0); //null;
                } finally {
                    if (res != null) {
                        res.close();
                    }
                    db.endTransaction();
                }
            }
        }
        return caneArr;
    }

    public CaneData[] seriesStart(long minStart) {
        long[] bounds = getCaneMinAndMaxTime();
        long max = bounds[1];
        long min = minStart;//bounds[1] - 500;     //Arbitrary initial spread
        return plotUpdate(min, max, 1);
    }


    /*
    public long[] getMinAndMaxForce() {
        SQLiteDatabase db = this.getReadableDatabase();
        long timeMin = -1;
        long timeMax = -1;

        synchronized (this) {
            db.beginTransaction();
            Cursor res = null;

            try {
                res = db.rawQuery("select MIN(force) from " + CANE_TABLE_FULL, null);
                res.moveToFirst();
                while (res.isAfterLast() == false) {
                    String testMin = res.getString(0);
                    timeMin = res.getLong(0);
                    res.moveToNext();
                }
                res = db.rawQuery("select MAX(force) from " + CANE_TABLE_FULL, null);
                res.moveToFirst();
                while (res.isAfterLast() == false) {
                    timeMax = res.getLong(0);
                    res.moveToNext();
                }
                db.setTransactionSuccessful();

            } catch (Exception e) {
                int failure = 1;
                String error = e.getMessage();
            } finally {
                db.endTransaction();
                if (res != null) {
                    res.close();
                }
            }
            return new long[]{timeMin, timeMax};
        }
    }
    */
}