package com.example.android.ble_android_cane;

/**
 * Created by Ian on 2016-05-10.
 */
public class CaneData {
    public long time;

    public double accx;
    public double accy;
    public double accz;
    public double gyrox;
    public double gyroy;
    public double gyroz;
    public double magx;
    public double magy;
    public double magz;
    public double sensor1;
    public double sensor2;
    public double sensor3;
    public double sensor4;
    public double sensor5;
    public double sensor6;
    public double sensor7;
    public double sensor8;

    public CaneData(long timeIn, double gyroxIn, double gyroyIn, double gyrozIn,
                    double accxIn, double accyIn, double acczIn, double magxIn, double magyIn, double magzIn,
                    double sensor1In, double sensor2In, double sensor3In, double sensor4In,
                    double sensor5In, double sensor6In, double sensor7In, double sensor8In) {
        time = timeIn;
        accx = accxIn;
        accy = accyIn;
        accz = acczIn;
        gyrox = gyroxIn;
        gyroy = gyroyIn;
        gyroz = gyrozIn;
        magx = magxIn;
        magy = magyIn;
        magz = magzIn;
        sensor1 = sensor1In;
        sensor2 = sensor2In;
        sensor3 = sensor3In;
        sensor4 = sensor4In;
        sensor5 = sensor5In;
        sensor6 = sensor6In;
        sensor7 = sensor7In;
        sensor8 = sensor8In;
    }

    public String toString(){
        String out = Long.toString(time)+" "+ Double.toString(gyrox)+" "+ Double.toString(gyroy)+" "+ Double.toString(gyroz)+
                " "+ Double.toString(accx)+" "+ Double.toString(accy)+" "+ Double.toString(accz)+
                " "+ Double.toString(magx)+" "+ Double.toString(magy)+" "+ Double.toString(magz)+
                " "+Double.toString(sensor1)+" "+ Double.toString(sensor2)+" "+ Double.toString(sensor3)+" "+ Double.toString(sensor4)+
                " "+ Double.toString(sensor5)+" "+ Double.toString(sensor6)+" "+ Double.toString(sensor7)+" "+ Double.toString(sensor8);
        return out;
    }
}
