package com.example.android.ble_android_cane;

/**
 * Created by Ian on 2016-05-10.
 */
public class CaneDataShort {
    public long time;
    public double force;
    public short accx;
    public short accy;
    public short accz;
    public short gyrox;
    public short gyroy;
    public short gyroz;
    public short pitch;
    public short roll;

    public CaneDataShort(long timeIn, short gyroxIn, short gyroyIn, short gyrozIn,
                         short accxIn, short accyIn, short acczIn,  short pitchIn, short rollIn, double forceIn) {
        time = timeIn;
        force = forceIn;
        accx = accxIn;
        accy = accyIn;
        accz = acczIn;
        gyrox = gyroxIn;
        gyroy = gyroyIn;
        gyroz = gyrozIn;
        pitch = pitchIn;
        roll = rollIn;
    }

    public String toString(){
        String out = Long.toString(time)+" "+ Double.toString(force)+" "+ Short.toString(accx)+
                " "+ Short.toString(accy)+" "+ Short.toString(accz)+" "+ Short.toString(gyrox)+
                " "+ Short.toString(gyroy)+" "+ Short.toString(gyroz)+" "+ Short.toString(pitch)+
                " "+ Short.toString(roll)+" ";
        return out;
    }
}
