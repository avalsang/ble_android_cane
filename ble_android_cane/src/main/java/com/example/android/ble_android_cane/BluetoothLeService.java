/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.android.ble_android_cane;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

import eu.darken.myolib.BaseMyo;
import eu.darken.myolib.Myo;
import eu.darken.myolib.MyoCmds;
import eu.darken.myolib.msgs.MyoMsg;
import eu.darken.myolib.processor.emg.EmgData;
import eu.darken.myolib.processor.emg.EmgProcessor;
import eu.darken.myolib.processor.imu.ImuData;
import eu.darken.myolib.processor.imu.ImuProcessor;

/**
 * Service for managing connection and data communication with a GATT server hosted on a
 * given Bluetooth LE device.
 */
public class BluetoothLeService extends Service implements EmgProcessor.EmgDataListener, ImuProcessor.ImuDataListener {
    private final static String TAG = BluetoothLeService.class.getSimpleName();

    //Reused by all devices
    private BluetoothManager mBluetoothManager;
    private BluetoothAdapter mBluetoothAdapter;

    //Specific to each device
    private LinkedList <String> mBluetoothDeviceAddress = new LinkedList<String>();
    private LinkedList <BluetoothGatt> mBluetoothGatt = new LinkedList<BluetoothGatt>();
    private DBHelper dbHelper;
    private CaneData[][] caneDataArr;
    private int caneArrRowSize = 5;
    private int caneArrSize = 100;
    private int index;
    private int indexCol;
    private byte[] imuTemp = null;
    private byte[] sensorTemp = null;
    private HashMap<String,Boolean> notificationsMap;
    private List<BluetoothGattCharacteristic> notifCharaList = null;
    private List<BluetoothGatt> notifGattList = null;
    public boolean recording = false;
    public boolean trigger = false;
    private int ind;
    private int[] emgArr = new int[8];
    public EmgProcessor mEmgProcessor;
    public ImuProcessor mImuProcessor;
    private EmgData[] emgDataArr;
    private int emgInd;
    private int emgDataArrSize = 200;
    private ImuData[] imuDataArr;
    private int imuInd;
    private int imuDataArrSize = 200;
    private Myo myo;
    private long prevTime;

    //Constants
    public static final int STATE_DISCONNECTED = BluetoothProfile.STATE_DISCONNECTED;   //0
    public static final int STATE_CONNECTING = BluetoothProfile.STATE_CONNECTING;       //1
    public static final int STATE_CONNECTED = BluetoothProfile.STATE_CONNECTED;         //2

    public final static String ACTION_GATT_CONNECTED =
            "com.example.bluetooth.le.ACTION_GATT_CONNECTED";
    public final static String ACTION_GATT_CONNECTING =
            "com.example.bluetooth.le.ACTION_GATT_CONNECTING";
    public final static String ACTION_GATT_DISCONNECTED =
            "com.example.bluetooth.le.ACTION_GATT_DISCONNECTED";
    public final static String ACTION_GATT_SERVICES_DISCOVERED =
            "com.example.bluetooth.le.ACTION_GATT_SERVICES_DISCOVERED";
    public final static String ACTION_DATA_AVAILABLE =
            "com.example.bluetooth.le.ACTION_DATA_AVAILABLE";
    public final static String EXTRA_DATA =
            "com.example.bluetooth.le.EXTRA_DATA";
    public final static String EXTRA_BYTES =
            "com.example.bluetooth.le.EXTRA_BYTES";
    public final static String DEVICE_ADDRESS =
            "com.example.bluetooth.le.DEVICE_ADDRESS";
    public final static String ACTION_FEEDBACK =
            "com.example.bluetooth.le.ACTION_FEEDBACK";
    public final static String EXTRA_LOAD =
            "com.example.bluetooth.le.EXTRA_LOAD";
    public final static String EXTRA_IND =
            "com.example.bluetooth.le.EXTRA_IND";

    // Service Constants for Bluefruit BLE shield
    public static final String UUID_SERVICE = "6e400001-b5a3-f393-e0a9-e50e24dcca9e";
    public static final String UUID_RX = "6e400003-b5a3-f393-e0a9-e50e24dcca9e";
    public static final String UUID_TX = "6e400002-b5a3-f393-e0a9-e50e24dcca9e";
    public static final String UUID_DFU = "00001530-1212-EFDE-1523-785FEABCD123";
    public static final int kTxMaxCharacters = 20;

    public final static UUID UUID_HEART_RATE_MEASUREMENT =
            UUID.fromString(SampleGattAttributes.HEART_RATE_MEASUREMENT);

    // Implements callback methods for GATT events that the app cares about.  For example,
    // connection change and services discovered.
    private final BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            String intentAction;
            int deviceNum = getDeviceNumber(gatt);
            if(deviceNum < 0){
                Log.d(TAG, "onConnectionStateChange: No devices found");
                return;
            }else {
                if (newState == BluetoothProfile.STATE_CONNECTED) {
                    intentAction = ACTION_GATT_CONNECTED;
                    broadcastUpdate(intentAction, gatt.getDevice().getAddress());
                    Log.i(TAG, "Connected to GATT server: " + gatt.getDevice().getAddress());
                    // Attempts to discover services after successful connection.
                    Log.i(TAG, "Attempting to start service discovery:" +
                            mBluetoothGatt.get(deviceNum).discoverServices());
                    gatt.requestConnectionPriority(BluetoothGatt.CONNECTION_PRIORITY_HIGH);
                } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                    intentAction = ACTION_GATT_DISCONNECTED;
                    Log.i(TAG, "Disconnected from GATT server.");
                    if(checkDeviceNotifications(gatt.getDevice().getAddress())){
                        if(recording){
                            recordingSwitch();
                        }
                    }
                    if(getMyoDevice(gatt) != null){
                        myo.removeProcessor(mImuProcessor);
                        myo.removeProcessor(mEmgProcessor);
                        myo.writeMode(MyoCmds.EmgMode.NONE, MyoCmds.ImuMode.NONE, MyoCmds.ClassifierMode.DISABLED,null);
                        myo.disconnect();
                        myo = null;
                    }
                    broadcastUpdate(intentAction, gatt.getDevice().getAddress());
                }
            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                //recordingSwitch(gatt);
                //Toast.makeText(getApplicationContext(),"Ready to start recording!",Toast.LENGTH_LONG);
                initializeMyo(gatt);
                broadcastUpdate(ACTION_GATT_SERVICES_DISCOVERED);
            } else {
                Log.w(TAG, "onServicesDiscovered received: " + status);
            }
        }

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt,
                                         BluetoothGattCharacteristic characteristic,
                                         int status) {
            Log.d(TAG, "onCharacteristicRead: Data received!");
            if (status == BluetoothGatt.GATT_SUCCESS) {
                broadcastUpdate(ACTION_DATA_AVAILABLE, characteristic);
                //parseData(characteristic.getValue());
                parseData(characteristic, ind);
            }
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt,
                                            BluetoothGattCharacteristic characteristic) {
            //Log.d(TAG, "onCharacteristicChanged: Data received!");
            broadcastUpdate(ACTION_DATA_AVAILABLE, characteristic);
            ind++;
            //parseData(characteristic.getValue());
            parseData(characteristic, ind);
            Log.d(TAG, "onCharacteristicChanged: Ind: "+ind+", Time: "+System.currentTimeMillis());

        }

        @Override
        public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status){
            super.onDescriptorWrite(gatt,descriptor,status);

            BluetoothGattCharacteristic target = null;
            String targetAddress = null;
            if(notifCharaList == null){
                return;
            }
            for(int i = 0; i < notifCharaList.size()-1; i++){
                BluetoothGattCharacteristic chara = notifCharaList.get(i);
                Log.d(TAG, "onDescriptorWrite: chara service: "+chara.getService().getUuid());
                Log.d(TAG, "onDescriptorWrite: desc service: "+descriptor.getCharacteristic().getService().getUuid());
                Log.d(TAG, "onDescriptorWrite: chara uuid: "+chara.getUuid());
                Log.d(TAG, "onDescriptorWrite: desc uuid: "+descriptor.getCharacteristic().getUuid());
                if(chara.getService().getUuid().equals(descriptor.getCharacteristic().getService().getUuid())
                        && chara.getUuid().equals(descriptor.getCharacteristic().getUuid())){
                        target = notifCharaList.get(i+1);
                        targetAddress = notifGattList.get(i+1).getDevice().getAddress();
                }
            }

            if(target == null){
                return;
            }

            Boolean temp = notificationsMap.get(target.getUuid().toString());
            if(temp == null){
                setCharacteristicNotification(target, true, targetAddress);
                notificationsMap.put(target.getUuid().toString(), true);
                //recording = true;
            }else{
                setCharacteristicNotification(target, !(temp.booleanValue()),targetAddress);
                notificationsMap.put(target.getUuid().toString(), !temp.booleanValue());
                //recording = !temp.booleanValue();
            }
        }
    };

    private void broadcastUpdate(final String action) {
        final Intent intent = new Intent(action);
        sendBroadcast(intent);
        //Log.d(TAG, "broadcastUpdate: Broadcast sent: "+action);
    }

    private void broadcastUpdate(final String action, String address) {
        final Intent intent = new Intent(action);
        intent.putExtra(DEVICE_ADDRESS, address);
        sendBroadcast(intent);
        //Log.d(TAG, "broadcastUpdate: Broadcast sent: "+action);
    }

    private void broadcastUpdate(final String action,
                                 final BluetoothGattCharacteristic characteristic) {
        final Intent intent = new Intent(action);

        //Log.d(TAG, "broadcastUpdate: Broadcast sent, characteristic: "+characteristic.getUuid());

        // For all profiles, writes the data formatted in HEX.
        final byte[] data = characteristic.getValue();
        Log.d(TAG, "broadcastUpdate: "+data.toString());
        if (data != null && data.length > 0) {
            final StringBuilder stringBuilder = new StringBuilder(data.length);
            for (byte byteChar : data)
                stringBuilder.append(String.format("%02X ", byteChar));
            //intent.putExtra(EXTRA_DATA, new String(data) + "\n" + stringBuilder.toString());
            intent.putExtra(EXTRA_DATA, stringBuilder.toString());
            intent.putExtra(EXTRA_BYTES, data);
            /*intent.putExtra(EXTRA_IND, ind);
            ind++;*/
            //Log.d(TAG, "broadcastUpdate: Characteristic Read: "+stringBuilder.toString());
        }
        sendBroadcast(intent);
    }

    public class LocalBinder extends Binder {
        BluetoothLeService getService() {
            return BluetoothLeService.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        // After using a given device, you should make sure that BluetoothGatt.close() is called
        // such that resources are cleaned up properly.  In this particular example, close() is
        // invoked when the UI is disconnected from the Service.
        close();
        return super.onUnbind(intent);
    }

    private final IBinder mBinder = new LocalBinder();

    /**
     * Initializes a reference to the local Bluetooth adapter.
     *
     * @return Return true if the initialization is successful.
     */
    public boolean initialize() {
        // For API level 18 and above, get a reference to BluetoothAdapter through
        // BluetoothManager.
        if (mBluetoothManager == null) {
            mBluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
            if (mBluetoothManager == null) {
                Log.e(TAG, "Unable to initialize BluetoothManager.");
                return false;
            }
        }

        mBluetoothAdapter = mBluetoothManager.getAdapter();
        if (mBluetoothAdapter == null) {
            Log.e(TAG, "Unable to obtain a BluetoothAdapter.");
            return false;
        }

        dbHelper = DBHelper.getInstance(this);
        caneDataArr = new CaneData[caneArrRowSize][caneArrSize];
        index = 0;
        indexCol = 0;

        notificationsMap = new HashMap<String,Boolean>();

        ind = 0;

        mEmgProcessor = new EmgProcessor();
        mEmgProcessor.addListener(this);
        mImuProcessor = new ImuProcessor();
        mImuProcessor.addListener(this);

        emgDataArr = new EmgData[emgDataArrSize];
        imuDataArr = new ImuData[imuDataArrSize];
        emgInd = 0;
        imuInd = 0;

        prevTime = System.currentTimeMillis();

        return true;
    }

    /**
     * Connects to the GATT server hosted on the Bluetooth LE device.
     *
     * @param addresses The device addresses of the destination devices.
     *
     * @return Return true if the connection is initiated successfully. The connection result
     *         is reported asynchronously through the
     *         {@code BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)}
     *         callback.
     */
    public boolean connect(final ArrayList<String> addresses) {
        if (mBluetoothAdapter == null || addresses == null) {
            Log.w(TAG, "BluetoothAdapter not initialized or unspecified address.");
            return false;
        }
        Log.d(TAG, "connect: Trying to connect to "+addresses.toString());
        if(addresses.size() < 1){
            Log.w(TAG, "connect: No devices selected.");
            return false;
        }
        for(int i = 0; i < addresses.size(); i++) {
            int deviceNum = getDeviceNumber(addresses.get(i));
            if (deviceNum == -2) {
                Log.d(TAG, "connect: Gatt list not initialized");
            }
            // Previously connected device.  Try to reconnect.
            if (deviceNum >= 0) {
                if(getConnectionStatus(addresses.get(i)) > STATE_DISCONNECTED) {
                        continue;
                }
                Log.d(TAG, "Trying to use an existing mBluetoothGatt for connection.");
                broadcastUpdate(ACTION_GATT_CONNECTING, addresses.get(i));
                if (mBluetoothGatt.get(deviceNum).connect()) {
                    Log.d(TAG, "connect: Successfully reconnected to " + mBluetoothGatt.get(deviceNum).getDevice().getAddress());
                    return true;
                } else {
                    Log.d(TAG, "connect: Could not reconnect to " + mBluetoothGatt.get(deviceNum).getDevice().getAddress());
                    mBluetoothGatt.get(deviceNum).disconnect();
                    broadcastUpdate(ACTION_GATT_DISCONNECTED,addresses.get(i));
                    return false;
                }
            }

            try {
                final BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(addresses.get(i));
                if (device == null) {
                    Log.w(TAG, "Device not found.  Unable to connect.");
                    return false;
                }

                broadcastUpdate(ACTION_GATT_CONNECTING, addresses.get(i));
                // We want to directly connect to the device, so we are setting the autoConnect
                // parameter to false.
                BluetoothGatt temp = device.connectGatt(this, false, mGattCallback);
                if(temp != null) {
                    mBluetoothGatt.add(temp);
                    Log.d(TAG, "Trying to create a new connection.");
                    mBluetoothDeviceAddress.add(addresses.get(i));
                    Log.d(TAG, "connect: Connecting to " + addresses.get(i));
                }else{
                    Log.d(TAG, "connect: Error connecting to " + addresses.get(i));
                    return false;
                }
            }catch(Exception e){
                Log.d(TAG, "connect: Cannot find address");
                return false;
            }
        }

        return true;
    }

    /**
     * Disconnects an existing connection or cancel a pending connection. The disconnection result
     * is reported asynchronously through the
     * {@code BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)}
     * callback.
     */
    public boolean disconnect(final ArrayList<String> addresses) {
        if (mBluetoothAdapter == null || addresses == null) {
            Log.w(TAG, "BluetoothAdapter not initialized or unspecified address.");
            return false;
        }
        Log.d(TAG, "connect: Trying to disconnect from "+addresses.toString());
        if(addresses.size() < 1){
            Log.w(TAG, "disconnect: No devices selected.");
            return false;
        }
        for(int i = 0; i < addresses.size(); i++) {
            int deviceNum = getDeviceNumber(addresses.get(i));
            if (deviceNum == -2) {
                Log.d(TAG, "disconnect: Gatt list not initialized");
            }

            if (deviceNum >= 0) {//mBluetoothGatt. != null) {
                if(getConnectionStatus(addresses.get(i)) < STATE_CONNECTED) {
                    broadcastUpdate(ACTION_GATT_DISCONNECTED, addresses.get(i));
                }

                mBluetoothGatt.get(deviceNum).disconnect();
            }

            final BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(addresses.get(i));
            if (device == null) {
                Log.w(TAG, "Device not found.  Unable to disconnect.");
                return false;
            }
        }

        return true;
    }

    /**
     * After using a given BLE device, the app must call this method to ensure resources are
     * released properly.
     */
    public void close() {
        if (mBluetoothGatt == null) {
            return;
        }
        for(int i = 0; i < mBluetoothGatt.size(); i++)
            mBluetoothGatt.get(i).close();
        mBluetoothGatt = null;
    }

    /**
     * Enables or disables notification on a give characteristic.
     *
     * @param characteristic Characteristic to act on.
     * @param enabled If true, enable notification.  False otherwise.
     */
    public boolean setCharacteristicNotification(BluetoothGattCharacteristic characteristic,
                                              boolean enabled, String address) {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
            return false;
        }
        if(characteristic == null){
            Log.w(TAG, "Characteristic not found");
            return false;
        }
        int deviceNum = getDeviceNumber(address);
        if(deviceNum < 0){
            Log.w(TAG, "Could not find device for characteristic notification");
            return false;
        }
        BluetoothGatt mGatt = mBluetoothGatt.get(deviceNum);

        mGatt.setCharacteristicNotification(characteristic, enabled);

        Log.d(TAG, "setCharacteristicNotification: Something's set for notification!");

        BluetoothGattDescriptor descriptor = characteristic.getDescriptor(
                UUID.fromString(SampleGattAttributes.CLIENT_CHARACTERISTIC_CONFIG));
        if(descriptor == null)
            return false;
        descriptor.setValue(BluetoothGattDescriptor.ENABLE_INDICATION_VALUE);
        descriptor.setValue(enabled ? BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE : BluetoothGattDescriptor.DISABLE_NOTIFICATION_VALUE);
        boolean writesuccess = mGatt.writeDescriptor(descriptor);
        Log.d(TAG, "setCharacteristicNotification: Notification set - " + Boolean.valueOf(enabled).toString() + " on " + (characteristic.getUuid()).toString());

        return writesuccess;
    }

    /**
     * Retrieves a list of supported GATT services on the connected device. This should be
     * invoked only after {@code BluetoothGatt#discoverServices()} completes successfully.
     *
     * @return A {@code List} of supported services.
     */
    public List<BluetoothGattService> getSupportedGattServices(String address) {
        if (mBluetoothGatt == null) return null;
        int devNum = getDeviceNumber(address);
        List<BluetoothGattService> services = null;
        if(devNum >= 0) {
            services = mBluetoothGatt.get(devNum).getServices();
        }
        return services;
    }

    public void writeCustomCharacteristic(String value) {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
            return;
        }
        /*check if the service is available on the device*/
        BluetoothGatt mGatt = null;
        BluetoothGattService mCustomService = null;
        for(int i=0; i < mBluetoothGatt.size(); i++){
            if(mCustomService == null) {
                mGatt = mBluetoothGatt.get(i);
                mCustomService = mGatt.getService(UUID.fromString(UUID_SERVICE));
            }
        }
        /*check if the service is available on the device*/
        if(mCustomService == null){
            Log.w(TAG, "Custom BLE Service not found");
            return;
        }
        /*get the write characteristic from the service*/
        BluetoothGattCharacteristic mWriteCharacteristic = mCustomService.getCharacteristic(UUID.fromString(UUID_TX));
        mWriteCharacteristic.setValue(value);
        if(mGatt.writeCharacteristic(mWriteCharacteristic) == false){
            Log.w(TAG, "Failed to write characteristic");
        }
    }

    public void writeCharacteristic(int value) {
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
            return;
        }
        /*check if the service is available on the device*/
        BluetoothGatt mGatt = null;
        BluetoothGattService mCustomService = null;
        for(int i=0; i < mBluetoothGatt.size(); i++){
            if(mCustomService == null) {
                mGatt = mBluetoothGatt.get(i);
                mCustomService = mGatt.getService(KnownUUIDs.getServiceUUID("Myo Command Service"));
            }
        }
        if(mCustomService == null){
            Log.w(TAG, "Myo command BLE Service not found");
            return;
        }
        /*get the write characteristic from the service*/
        BluetoothGattCharacteristic mWriteCharacteristic = mCustomService.getCharacteristic(KnownUUIDs.getCharacteristicUUID("Command Chara"));
        //mWriteCharacteristic.setValue(value);
        if(mGatt.writeCharacteristic(mWriteCharacteristic) == false){
            Log.w(TAG, "Failed to write characteristic");
        }
    }

    private int getDeviceNumber(BluetoothGatt gatt){
        if(mBluetoothGatt == null){
            return -2;
        }
        for(int i = 0; i < mBluetoothGatt.size(); i++){
            if(gatt.getDevice().getAddress().equals(mBluetoothGatt.get(i).getDevice().getAddress())){
                Log.d(TAG, "getDeviceNumber: "+i);
                return i;
            }
        }
        return -1;
    }
    private int getDeviceNumber(String address){
        if(mBluetoothGatt == null){
            return -2;
        }
        for(int i = 0; i < mBluetoothGatt.size(); i++){
            if(address == mBluetoothGatt.get(i).getDevice().getAddress()){
                Log.d(TAG, "getDeviceNumber: "+i);
                return i;
            }
        }
        return -1;
    }
    public BluetoothDevice getDevice(String address){
        if(mBluetoothGatt == null){
            return null;
        }
        for(int i = 0; i < mBluetoothGatt.size(); i++){
            if(address == mBluetoothGatt.get(i).getDevice().getAddress()){
                Log.d(TAG, "getDeviceNumber: "+i);
                return mBluetoothGatt.get(i).getDevice();
            }
        }
        return null;
    }

    public ArrayList<BluetoothDevice> getAllDevices(){
        ArrayList<BluetoothDevice> arrayOut = new ArrayList<BluetoothDevice>();
        Log.d(TAG, "Connected devices: ");
        for(int i = 0; i < mBluetoothGatt.size(); i++){
            Log.d(TAG,mBluetoothGatt.get(i).getDevice().getAddress());
            arrayOut.add(mBluetoothGatt.get(i).getDevice());
        }
        return arrayOut;
    }

    public ArrayList<BluetoothDeviceItem> getAllDeviceItems(){
        ArrayList<BluetoothDeviceItem> arrayOut = new ArrayList<BluetoothDeviceItem>();
        Log.d(TAG, "Connected devices: ");
        BluetoothDevice temp = null;
        for(int i = 0; i < mBluetoothGatt.size(); i++){
            temp = mBluetoothGatt.get(i).getDevice();
            Log.d(TAG,temp.getAddress());
            arrayOut.add(new BluetoothDeviceItem(temp, getConnectionStatus(temp.getAddress())));
        }
        return arrayOut;
    }

    public int getConnectionStatus(String address){
        int deviceNum = getDeviceNumber(address);
        if(deviceNum < 0)
            return 0;
        return mBluetoothManager.getConnectionState(getDevice(address), BluetoothProfile.GATT);

    }

    public void removeDevice(String address){
        int deviceNum = getDeviceNumber(address);

        if(checkDeviceNotifications(mBluetoothGatt.get(deviceNum).getDevice().getAddress())){
            if(recording){
                //recordingSwitch();
                recording = false;
            }
        }
        ArrayList<String> arr = new ArrayList<String>();
        arr.add(address);
        disconnect(arr);

        removeCharacteristics(mBluetoothGatt.get(deviceNum));
        //Potential for race condition if other methods are modifying these values
        mBluetoothGatt.set(deviceNum,null);
        mBluetoothDeviceAddress.set(deviceNum,null);
        mBluetoothGatt.remove(deviceNum);
        mBluetoothDeviceAddress.remove(deviceNum);
    }

    //Method for two-characteristic layout
    /*private void parseData(BluetoothGattCharacteristic characteristic, int indi) {
        if (KnownUUIDs.getCharacteristicName(characteristic.getUuid().toString()).equalsIgnoreCase("Cane Sensors")) {

            BluetoothGattService caneService = characteristic.getService();
            BluetoothGattCharacteristic imuChara = null;
            for (BluetoothGattCharacteristic chara : caneService.getCharacteristics()) {
                if (KnownUUIDs.getCharacteristicName(chara.getUuid().toString()).equalsIgnoreCase("Cane IMU")) {
                    imuChara = chara;
                }
            }

            if (imuChara == null) {
                return;
            }

            if (characteristic.getValue() != null && imuChara.getValue() != null) {
                CaneData canedata = DataRead.translateData(imuChara.getValue(), characteristic.getValue(), System.currentTimeMillis());
                Log.d(TAG, "parseData: Ind: "+indi+", Time: "+System.currentTimeMillis());
                renderFeedback(canedata,indi);
                if (canedata == null) {
                    Log.d(TAG, "parseData: IMU issue: " + Arrays.toString(characteristic.getValue()));
                    return;
                }
                if(trigger){
                    canedata.sensor2 = 1;
                }
                synchronized (this) {
                    caneDataArr[index] = canedata;
                    index++;
                    if (index == caneArrSize) {
                        dbHelper.caneInsertLoop(caneDataArr);
                        index = 0;
                    }
                }
            }

        } else if (KnownUUIDs.getCharacteristicName(characteristic.getUuid().toString()).equalsIgnoreCase("Cane IMU")) {
            return;
        }
    }*/

    private void parseData(BluetoothGattCharacteristic characteristic, int indi) {
        //TODO: seperate databases for IMU and sensors
        if (KnownUUIDs.getCharacteristicName(characteristic.getUuid().toString()).equalsIgnoreCase("Cane IMU")) {

            if (characteristic.getValue() != null) {
                CaneData canedata = DataRead.translateData(characteristic.getValue(), System.currentTimeMillis());
                Log.d(TAG, "parseData: Ind: " + indi + ", Time: " + System.currentTimeMillis());
                renderFeedback(canedata, indi);
                if (canedata == null) {
                    Log.d(TAG, "parseData: IMU issue: " + Arrays.toString(characteristic.getValue()));
                    return;
                }
                if (trigger) {
                    canedata.magy = 1;  //magx is used for adc load, magy for trigger, magz for calculated load
                }
                synchronized (this) {
                    caneDataArr[indexCol][index] = canedata;
                    index++;
                    if (index == caneArrSize) {
                        dbHelper.caneInsertLoop(caneDataArr[indexCol]);
                        index = 0;
                    }
                }
            }
        }else if(KnownUUIDs.getCharacteristicName(characteristic.getUuid().toString()).equalsIgnoreCase("RX Buffer")){
            if (characteristic.getValue() != null) {
                CaneData canedata = DataRead.translatePison(characteristic.getValue(), System.currentTimeMillis());
                //renderFeedback(canedata, indi);
                if (canedata == null) {
                    Log.d(TAG, "parseData: pison issue: " + Arrays.toString(characteristic.getValue()));
                    return;
                }
                synchronized (this) {
                    caneDataArr[indexCol][index] = canedata;
                    caneDataArr[indexCol][index].magz = indexCol;
                    index++;
                    if (index == caneArrSize) {
                        //dbHelper.caneInsertLoop(caneDataArr[indexCol]);
                        Log.d(TAG, "parseData: Starting async column: "+indexCol);
                        new DbInsert().execute(indexCol);
                        index = 0;
                        indexCol++;
                        if(indexCol >= caneArrRowSize){
                            indexCol = 0;
                        }
                    }
                }
            }
        }

    }

    public boolean checkDeviceNotifications(String address){
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
            return false;
        }
        int devicenum = getDeviceNumber(address);
        BluetoothGatt mGatt = mBluetoothGatt.get(devicenum);
        boolean found = false;
        for(BluetoothGattService service: mGatt.getServices()){
            for(BluetoothGattCharacteristic characteristic: service.getCharacteristics()){
                if(checkIfNotifiable(characteristic)){
                    //setCharacteristicNotification(characteristic, true, mGatt.getDevice().getAddress());
                    found = true;
                }
            }
        }
        return found;
    }

    public boolean checkIfNotifiable(BluetoothGattCharacteristic characteristic){
        return KnownUUIDs.checkNotifiable(characteristic.getUuid().toString());
    }

    public boolean changeUartComms(boolean currentState){ //true if notifications state switch successful, false ow
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
            return false;
        }

        BluetoothGatt mGatt = null;
        BluetoothGattService mCustomService = null;
        for(int i=0; i < mBluetoothGatt.size(); i++){
            if(mCustomService == null) {
                mGatt = mBluetoothGatt.get(i);
                mCustomService = mGatt.getService(UUID.fromString(UUID_SERVICE));
            }
        }
        /*check if the service is available on the device*/
        if(mCustomService == null){
            Log.w(TAG, "Custom BLE Service not found");
            return false;
        }
        /*start/stop UART notifications*/
        BluetoothGattCharacteristic rxCharacteristic = mCustomService.getCharacteristic(UUID.fromString(UUID_RX));
        if (rxCharacteristic != null) {
            setCharacteristicNotification(rxCharacteristic, !currentState, mGatt.getDevice().getAddress());
            Log.d(TAG, "changeUartComms: Rx notifications successfully switched");
            return true;
        }
        Log.d(TAG, "changeUartComms: Error retrieving Rx channel");
        return false;
    }

    public boolean recordingSwitch(){
        if (mBluetoothAdapter == null || mBluetoothGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
            return false;
        }
        boolean switchie = false;
        notifCharaList = new ArrayList<BluetoothGattCharacteristic>();
        notifGattList = new ArrayList<BluetoothGatt>();

        for(BluetoothGatt gatt: mBluetoothGatt){
            for(BluetoothGattService service: gatt.getServices()){
                for(BluetoothGattCharacteristic characteristic: service.getCharacteristics()){
                    if(checkIfNotifiable(characteristic)){
                        notifGattList.add(gatt);
                        notifCharaList.add(characteristic);
                    }
                }
            }
        }

        int counter = 0;
        int arrLen = notifGattList.size();
        for(int i = 0; i < arrLen; i++){
            if(getConnectionStatus(notifGattList.get(i-counter).getDevice().getAddress()) != STATE_CONNECTED){
                notifGattList.remove(i-counter);
                notifCharaList.remove(i-counter);
                counter++;
            }
        }

        if(notifCharaList.size() < 1){
            recording = false;
            return false;
        }

        //TODO: make this a for loop so everything has its recording status changed? Or just ditch the map altogether
        Boolean temp = notificationsMap.get(notifCharaList.get(0).getUuid().toString());
        boolean notifSuccess = false;
        if(temp == null){
            notifSuccess = setCharacteristicNotification(notifCharaList.get(0), true, notifGattList.get(0).getDevice().getAddress());
            if(notifSuccess){
                notificationsMap.put(notifCharaList.get(0).getUuid().toString(), true);
                recording = true;
            }
        }else{
            notifSuccess = setCharacteristicNotification(notifCharaList.get(0), !(temp.booleanValue()),notifGattList.get(0).getDevice().getAddress());
            if(notifSuccess){
                notificationsMap.put(notifCharaList.get(0).getUuid().toString(), !temp.booleanValue());
                recording = !temp.booleanValue();
                if(recording == false){ //Flush remaining data in array by resetting index
                    index = 0;
                }
            }
        }

        //Turn on or off myo recording depending on system state.
        if(recording){
            if(myo != null){
                myo.writeMode(MyoCmds.EmgMode.FILTERED, MyoCmds.ImuMode.ALL, MyoCmds.ClassifierMode.DISABLED, null);
            }
        }else{
            if(myo != null){
                myo.writeMode(MyoCmds.EmgMode.NONE, MyoCmds.ImuMode.NONE, MyoCmds.ClassifierMode.DISABLED, null);
            }
        }
        return notifSuccess;
    }

    public boolean removeCharacteristics(BluetoothGatt gatt){
        boolean switchie = false;
        if (mBluetoothAdapter == null || gatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
            return false;
        }
        for(BluetoothGattService service: gatt.getServices()){
            for(BluetoothGattCharacteristic characteristic: service.getCharacteristics()){
                if(checkIfNotifiable(characteristic)){
                    Boolean temp = notificationsMap.get(characteristic.getUuid().toString());
                    if(temp == null){
                        continue;
                    }else{
                        notificationsMap.remove(characteristic.getUuid().toString());
                        switchie = true;
                    }
                }
            }
        }
        return switchie;
    }

    private void renderFeedback(CaneData canedata,int indi){
        final Intent intent = new Intent(ACTION_FEEDBACK);
        //intent.putExtra(EXTRA_LOAD, canedata.sensor1);
        intent.putExtra(EXTRA_LOAD, canedata.magz);
        intent.putExtra(EXTRA_IND, indi);
        sendBroadcast(intent);
        Log.d(TAG, "renderFeedback: Ind: "+indi+", Time: "+System.currentTimeMillis());
    }

    public BluetoothDevice getMyoDevice(BluetoothGatt gatt){
        for(BluetoothGattService service: gatt.getServices()){
            if(KnownUUIDs.getServiceName(service.getUuid().toString()).equals("Myo Command Service"))
                return gatt.getDevice();
        }
        return null;
    }

    public BluetoothDevice getMyoDevice(){
        for(BluetoothGatt gatt: mBluetoothGatt) {
            for (BluetoothGattService service : gatt.getServices()) {
                if (KnownUUIDs.getServiceName(service.getUuid().toString()).equals("Myo Command Service"))
                    return gatt.getDevice();
            }
        }
        return null;
    }

    @Override
    public void onNewEmgData(final EmgData emgData) {
        emgDataArr[emgInd] = emgData;
        emgInd++;
        if(emgInd == emgDataArrSize){
            dbHelper.emgInsertLoop(emgDataArr);
            emgInd = 0;
        }
    }

    @Override
    public void onNewImuData(final ImuData imuData) {
        imuDataArr[imuInd] = imuData;
        imuInd++;
        if(imuInd == imuDataArrSize){
            dbHelper.imuInsertLoop(imuDataArr);
            imuInd = 0;
        }
    }

    private void initializeMyo(BluetoothGatt gatt){
        BluetoothDevice dev = getMyoDevice(gatt);
        if(dev == null){
            Toast.makeText(this,"Not connected to a Myo band!",Toast.LENGTH_SHORT).show();
            myo = null;
        }else{
            myo = new Myo(this,dev);
            Object temp = myo.getConnectionState();
            if(myo.getConnectionState().equals(BaseMyo.ConnectionState.DISCONNECTED)) {
                myo.connect();
                myo.writeMode(MyoCmds.EmgMode.FILTERED, MyoCmds.ImuMode.ALL, MyoCmds.ClassifierMode.DISABLED, null);
                myo.writeSleepMode(MyoCmds.SleepMode.NEVER,null);
                myo.writeUnlock(MyoCmds.UnlockType.HOLD, new Myo.MyoCommandCallback() {
                    @Override
                    public void onCommandDone(Myo myo, MyoMsg msg) {
                        myo.writeVibrate(MyoCmds.VibrateType.LONG, null);
                    }
                });
            }
            myo.addProcessor(mEmgProcessor);
            myo.addProcessor(mImuProcessor);
        }
    }

    private class DbInsert extends AsyncTask<Integer, Integer, String> {

        protected String doInBackground(Integer... column) {
            String message = dbHelper.caneInsertLoop(caneDataArr[column[0]]);
            if(!message.equalsIgnoreCase("Clear")){
                Log.d(TAG, "doInBackground: Error inserting column "+column[0]+", "+message);
            }

            return column.toString();
        }

        protected void onPostExecute(String result) {
            //Can use a map of usable columns if it gets dicey with locks
            Log.d(TAG, "onPostExecute: Wrote column "+result);
        }
    }
}
